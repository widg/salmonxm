<?php

return [
	'baseUri' => env('FRONTPAD_BASE_URI', 'https://app.frontpad.ru/api/index.php?'),
	'secret' => env('FRONTPAD_SECRET')
];