<?php
/**
 * Created by PhpStorm.
 * User: surrealistik
 * Date: 21.04.18
 * Time: 19:26
 */

namespace App;

use GuzzleHttp\Client;

class FrontPad
{
	protected $client;
	protected $methods = [
		'new_order',
		'get_status',
		'get_client',
		'get_certificate',
		'get_products'
	];
	
	/**
	 * FrontPad constructor.
	 */
	function __construct()
	{
		$this->client = new Client([
			'base_uri' => config('frontpad.baseUri')
		]);
	}
	
	/**
	 * @param $name
	 * @param $arguments
	 * @return mixed
	 * @throws \Exception
	 */
	public function __call($name, $arguments)
	{
		if (array_has($this->methods, $name)) {
			throw new \Exception("Method {$name} not defined");
		}
		$result = $this->client->request('POST', 'index.php',
			[
				'query' => $name,
				'form_params' =>
					array_merge([
					'secret' => config('frontpad.secret')
					], $arguments[0])
			]);
		$data = (string) $result->getBody();
		
		return json_decode($data);
	}
}