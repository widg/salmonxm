<?php

namespace App;

class Cart
{
	public $items = null;
    //public $form_params = null;
	public $totalPrice = 0;
	

	/**
	 * Constructor for the Cart that allows you to to reassing old cart values
	 * to new cart object.
	 * @param Cart $oldCart 
	 */
	public function __construct($oldCart)
	{
		if ($oldCart) {
			$this->items = $oldCart->items;
			//$this->form_params = $oldCart->form_params;
			$this->totalPrice = $oldCart->totalPrice;
		}
	}

	public function getItems()
	{
		return $this->items;
	}

	public function getTotalPrice()
	{
		return $this->totalPrice;
	}
			
	/**
	 * Add to cart given item and recalculate total price.
	 * @param Product $item 
	 * @param Integer $id   Product ID
	 */
	public function addToCart($item, $id)
	{
		$storedItem = [
			'item' => $item,
			'quantity' => 0,
            'product' => $item->article,
			'price' => $item->price,
		];
//		$form_param = [
//            'product' => $item->article,
//            'product_kol' => 0
//        ];

		if ($this->items) {
			if (array_key_exists($id, $this->items)) {
				$storedItem = $this->items[$id];
			}
		}
//        if ($this->form_params) {
//            if (array_key_exists($id, $this->items)) {
//                $form_param = $this->form_params[$id];
//            }
//        }

		$storedItem['quantity']++;
        //$form_param['product_kol']++;
		$storedItem['price'] = $storedItem['quantity'] * $item->price;
		$this->items[$id] = $storedItem;
		//$this->form_params[$id] = $form_param;
		$this->totalPrice += $item->price;

	}
    /**  [ 'form_params' => [
        "secret" =>"7z5KHG67AbRRH7TZEaEz48ryfS7arfAiEsZ",
         *  product[0]=10000
            product_kol[0]=1
            product[1]=10001
            product_kol[1]=1
            product_mod[1]=0
            product[2]=10002
            product_kol[2]=1
            product_mod[2]=0
            product[3]=10003
            product_kol[3]=1
            "product[0]" => "2",
            "product_kol" => "1",
        "street" => "TEST",
        "home" => "6",
        "pod" => "2",
        "et" => "5",
        "apart" => "72",
        "phone" => "89998887766",
        "name" => "test"
    ]];
	/**
	 * Delete product with that id from cart and recalculate total price.
	 * @param  Integer $id Product ID
	 */
	public function deleteFromCart($id)
	{
		$this->totalPrice -= $this->items[$id]['price'];

		unset($this->items[$id]);		
		//unset($this->$form_param[$id]);
	}
}