<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Review;

/**
 * Class DashboardController.
 */
class DashboardController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('backend.dashboard');
    }

    public function products()
    {
        return view('backend.catalog.products');
    }

    public function product_add()
    {
        return view('backend.catalog.product_add');
    }

    public function reviews()
    {
        $review = Review::paginate(20);
        return view('backend.reviews.index', ['$reviews' => $review]);
    }

    public function addguestbook(Request $request)
    {

//        $this->validate($request, ['foto' => 'image']);
//        $imageName = 'default.png';
//        if($request->hasFile('foto')) {
//            $foto = $request->file('foto');
//            $imageName = str_random(12) . '.' . $foto->getClientOriginalExtension();
//            Image::make($foto)->save(public_path('images/guestbook/foto/' . $imageName)); //fit(400, 400)->
//
//            //$user->update(['foto' => $imageName]);
//        }
//
//        $data = [
//            'name' => $request->name,
//            'number' => $request->number,
//            'phone' => $request->phone,
//            'comment' => $request->comment,
//            'foto' => $imageName,
//            'checkbox_mail' => $request->checkbox_mail,
//            'checkbox_phone' => $request->checkbox_phone,
//        ];
//
//        Guestbook::create($data);
//
//        return redirect('guestbook')->with('success_message', 'Отзыв успешно отправлен!');
    }
}
