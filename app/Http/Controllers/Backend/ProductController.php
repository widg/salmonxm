<?php

namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Product;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $bids = Product::all();

        return response()->json([
            'bids' => $bids
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $name = mb_convert_case(trim($request->name), MB_CASE_TITLE);
        $imageName = str_random(12) . '.' . $request->file('image')->getClientOriginalExtension();
        $pathToImages = public_path('images/products/');

        $request->file('image')->move($pathToImages, $imageName);

        if ($request->diameter == '') {
            $request->diameter = NULL;
        }
        $data = [
            'category' => $request->category,
            'name' => $name,
            'price' => $request->price,
            'weight' => $request->weight,
            'diameter' => $request->diameter,
            'pathToImage' => 'images/products/' . $imageName,
            'composition' => $request->composition,
            'description' => $request->description,
            'article'=> $request->article,
        ];

        Product::create($data);

        return redirect('/admin/products/')->with('success_message', 'Товар был успешно добавлен!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $bids = Product::find($id);

        $bids->update($request->all());

        return response()->json([
            'message' => 'Bid updated successfully'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Product::find($id)->delete();

        return response()->json([
            'message' => 'Bid deleted successfully'
        ]);
    }
}
