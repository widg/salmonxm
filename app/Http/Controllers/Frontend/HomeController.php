<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Product;
use App\Cart;
use Auth;
use App\Http\Requests\ProductRequest;
use App\Order;
use Illuminate\Http\Request;
use Session;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

/**
 * Class HomeController.
 */
class HomeController extends Controller
{
	/**
	 * @return \Illuminate\View\View
	 */
	public function index()
	{
		//return view('frontend.index');
		//$products = Product::all();
		
		return view('frontend.index');//->with('products', $products);
	}
	
	/**
	 * Get all information about product and send it to product page.
	 *
	 * @param string $category
	 * @param integer $id
	 * @return Response
	 */
	public function show($category, $id)
	{
		$product = Product::findOrFail($id);
		
		return view('frontend.main.productDescription')->with('product', $product);
		
	}
	
	/**
	 * Show the page with given category.
	 *
	 * @param string $category
	 * @return $Response
	 */
	public function showCategory($category)
	{
		$products = Product::where('category', $category)->get();
		if (Auth::check()) {
			$user = auth()->user();
			return view('frontend.main.showCategory')->with([
				'user' => $user->find(auth()->user()->id),
				'products' => $products
			]);
		} else {
			return view('frontend.main.showCategory')->with('products', $products);
		}
		
		//$user = find(auth()->user()->first());
		
	}
	
	/**
	 * Show the create product page.
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function create()
	{
		return view('frontend.main.createProduct');
	}
	
	/**
	 * Handle information about new product and store it.
	 *
	 * @param ProductRequest $request
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function store(ProductRequest $request)
	{
		$name = mb_convert_case(trim($request->name), MB_CASE_TITLE);
		$imageName = str_random(12) . '.' . $request->file('image')->getClientOriginalExtension();
		$pathToImages = public_path('images/');
		
		$request->file('image')->move($pathToImages, $imageName);
		
		if ($request->diameter == '') {
			$request->diameter = NULL;
		}
		$data = [
			'category' => $request->category,
			'name' => $name,
			'price' => $request->price,
			'weight' => $request->weight,
			'diameter' => $request->diameter,
			'pathToImage' => 'images/' . $imageName,
			'composition' => $request->composition,
			'description' => $request->description,
			'article' => $request->article,
		];
		
		Product::create($data);
		
		return redirect('/')->with('success_message', 'Товар был успешно добавлен!');
	}
	
	/**
	 * Find and add choosen product to cart.
	 * @param Request $request
	 * @param Product $product
	 * @return \Illuminate\Http\JsonResponse
	 * @throws \Throwable
	 */
	public function addToCart(Request $request, Product $product)
	{
		$product = $product->find($request->productId);
		$oldCart = Session::has('cart') ? Session::get('cart') : null; // check if we already got cart in session
		$cart = new Cart($oldCart);
		$cart->addToCart($product, $product->id);
		Session::put('cart', $cart);
		
		$returnHTML = view('frontend.cart.cart')->render();
		return response()->json([
			'success' => 'Успех',
			'html' => $returnHTML
		]);
	}
	
	/**
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 * @throws \Throwable
	 */
	public function deleteFromCart(Request $request)
	{
		$oldCart = Session::get('cart');
		$cart = new Cart($oldCart);
		$cart->deleteFromCart($request->productId);
		
		if (empty($cart->items)) {
			Session::forget('cart');
		} else {
			Session::put('cart', $cart);
		}
		
		$returnHTML = view('frontend.cart.cart')->render();
		return response()->json([
			'success' => 'Элемент удалён',
			'html' => $returnHTML
		]);
	}
	
	/**
	 * @param Request $request
	 * @param Order $order
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function buyProducts(Request $request, Order $order)
	{
		$cart = Session::get('cart');
		
		Stripe::setApiKey('sk_test_WLRHLuQRqs7pJYIgOiOAVuHF');
		
		try {
			$charge = Charge::create([
				'amount' => round(($cart->totalPrice / 60), 2) * 100,
				'currency' => 'usd',
				'source' => $request['stripeToken'],
				'description' => 'Test Charge'
			]);
			
			$order->cart = serialize($cart);
			$order->address = $request['address'];
			$order->payment_id = $charge->id;
			auth()->user()->orders()->save($order);
		} catch (\Exception $error) {
			return redirect('/')->with('error', $error->getMessage());
		}
		
		Session::forget('cart');
		return redirect('/')->with('success_message', 'Покупка успешно завершена!');
		
	}
	
	/**
	 * @param Request $request
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function buyProducts2(Request $request)
	{
		$cart = $request->session()->get('cart');
		
		$collection = collect(collect($cart)->get('items'))->map(function ($item) {
			return collect($item)->except('price');
		});
		
		$arr["product"] = $collection->pluck('product')->toArray();
		$arr["product_kol"] = $collection->pluck('quantity')->toArray();
		
		$map = collect($arr)->map(function ($data, $id) {
			return collect($data)
				->keyBy(function ($item, $key) use ($id) {
					return "{$id}[{$key}]";
				});
		})
			->collapse()
			->merge(
				$request->only('street', 'home', 'pod', 'et', 'apart', 'phone', 'name', 'card', 'descr', 'person')
			);
		
		$obj = \FrontPad::new_order($map->all());
		$request->session()->flush();
		
		return redirect()->back()->with('success_message', 'Ваш заказ: ' . $obj->order_number);
		
		
	}
}
