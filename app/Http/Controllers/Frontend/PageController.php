<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Guestbook;
use Illuminate\Http\Request;
use Image;
use Session;

class PageController extends Controller
{
    // /bonus_card
    public function bonus_card()
    {
        return view('frontend.bonus_card');
    }
	// /new_vacancy
	public function new_vacancy()
    {
        return view('frontend.new_vacancy');
    }
	// /conditions
	public function conditions()
    {
        return view('frontend.conditions');
    }
    //terms-use
    public function termsuse()
    {
        return view('frontend.terms-use');
    }
	// /guestbook
	public function guestbook()
    {
        return view('frontend.guestbook');
    }
    public function guestbooks()
    {
        if (Auth::check()) {
            $iduser = auth()->user()->id;
            if ($iduser == 1){
                $book = Guestbook::paginate(20);
                return view('frontend.admin.guestbooks', ['guestbooks' => $book]);
            }

        } else {
            return Response::view('frontend.errors.503', array(), 404);
        }

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function addguestbook(Request $request)
    {
        //$user = auth()->user();

        $this->validate($request, ['foto' => 'image']);
        $imageName = 'default.png';
        if($request->hasFile('foto')) {
            $foto = $request->file('foto');
            $imageName = str_random(12) . '.' . $foto->getClientOriginalExtension();
            Image::make($foto)->save(public_path('images/guestbook/foto/' . $imageName)); //fit(400, 400)->

            //$user->update(['foto' => $imageName]);
        }

        $data = [
            'name' => $request->name,
            'number' => $request->number,
            'phone' => $request->phone,
            'comment' => $request->comment,
            'foto' => $imageName,
            'checkbox_mail' => $request->checkbox_mail,
            'checkbox_phone' => $request->checkbox_phone,
        ];

        Guestbook::create($data);

        return redirect('guestbook')->with('success_message', 'Отзыв успешно отправлен!');
        //dd($request);
        //return view('guestbook');
    }
}
