<?php
/**
 * Created by PhpStorm.
 * User: surrealistik
 * Date: 21.04.18
 * Time: 19:57
 */

namespace App\Contracts;

use App\FrontPad;
use Illuminate\Support\Facades\Facade;

class FrontPadFacade extends Facade
{
	/**
	 * Get the registered name of the component.
	 *
	 * @return string
	 */
	protected static function getFacadeAccessor()
	{
		return 'FrontPad';
	}
}