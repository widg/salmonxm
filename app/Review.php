<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    //
    protected $fillable = [
        'name',
        'number',
        'phone',
        'comment',
        'foto',
        'checkbox_mail',
        'checkbox_phone'
    ];
}
