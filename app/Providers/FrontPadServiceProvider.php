<?php

namespace App\Providers;

use App\FrontPad;
use Illuminate\Support\ServiceProvider;

class FrontPadServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
		$this->app->bind('FrontPad', FrontPad::class);
    }
}
