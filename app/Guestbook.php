<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Guestbook extends Model
{
    protected $fillable = [
        'name',
        'number',
        'phone',
        'comment',
        'foto',
        'checkbox_mail',
        'checkbox_phone'
    ];
    protected $table = 'guestbook';
}
