<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    protected $fillable = [
        'category',
        'name',
        'price',
        'weight',
        'composition',
        'description',
        'pathToImage',
        'diameter',
        'article'
    ];

    public $timestamps = false;
}
