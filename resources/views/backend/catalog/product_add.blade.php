@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('strings.backend.dashboard.title'))

@section('content')
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <strong>Добавить продукт в каталог</strong>
                </div><!--card-header-->
                <div class="card-block">
                    <div class="row">
                        @include('backend.errors.errors')
                        {!! Form::open(['files' => true, 'url' => 'bids']) !!}
                        <div class="form-group">
                            <div class="col">
                                {!! Form::label('name', 'Название товара:') !!}
                                {!! Form::text('name', null, ['class' => 'form-control']) !!}
                                <hr>
                                {!! Form::label('category', 'Категория:') !!}
                                Пицца {!! Form::radio('category', 'pizza')  !!}
                                Суши {{ Form::radio('category', 'sushi') }}
                                Напитки {{ Form::radio('category', 'drinks') }}
                                Соусы {{ Form::radio('category', 'sausages') }}
                                Горячие роллы {{ Form::radio('category', 'gorjachie-rolly') }}
                                Сеты {{ Form::radio('category', 'sety') }}
                                Роллы {{ Form::radio('category', 'rolly') }}
                                Десерты {{ Form::radio('category', 'deserty') }}
                                Закуски {{ Form::radio('category', 'zakuski') }}
                                Запечённые роллы {{ Form::radio('category', 'zapechennye-rolly') }}
                                Доп {{ Form::radio('category', 'dop') }}
                                <br>
                                <hr>
                                <br>
                                {!! Form::label('composition', 'Ингридиенты:') !!}
                                {!! Form::textarea('composition', null, ['class' => 'form-control']) !!}
                            </div>
                            <div class="col">
                                {!! Form::label('price', 'Цена товара:') !!}
                                {!! Form::text('price', null, ['class' => 'form-control']) !!}
                                <hr>
                                {!! Form::label('weight', 'Вес:') !!}
                                {!! Form::text('weight', null, ['class' => 'form-control']) !!}
                                <hr>
                                {!! Form::label('description', 'Описание товара:') !!}
                                {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
                            </div>
                            <div class="col">
                                {!! Form::label('diameter', 'Диаметр (если пицца):') !!}
                                {!! Form::text('diameter', null, ['class' => 'form-control']) !!}
                                <hr>
                                <h2>Изображение:</h2>
                                <div class="input-group">
                                    <label class="input-group-btn">
                                <span class="btn btn-primary">
                                    Browse&hellip; <input type="file" style="display: none;" multiple name="image">
                                </span>
                                    </label>
                                    {!! Form::text('pathToImage', null, ['class' => 'form-control']) !!}
                                </div>
                                <br><br><br>
                                <hr>
                                {!! Form::label('article', 'Артикул:') !!}
                                {!! Form::text('article', null, ['class' => 'form-control']) !!}
                                <br>
                                {!! Form::submit('Добавить товар', ['class' => 'btn btn-success form-control']) !!}
                            </div><!--col-md-4-->
                        </div><!--form-group-->
                        {!! Form::close() !!}
                    </div><!--col-md-12-->
                </div><!--card-block-->
            </div><!--card-->
        </div><!--col-->
    </div><!--row-->
@endsection