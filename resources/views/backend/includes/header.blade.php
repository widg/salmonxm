<header class="app-header navbar">
    <button class="navbar-toggler mobile-sidebar-toggler d-lg-none mr-auto" type="button">☰</button>
    <a class="navbar-brand" href="#"></a>
    <button class="navbar-toggler sidebar-minimizer d-md-down-none" type="button">☰</button>

    <ul class="nav navbar-nav d-md-down-none">
        <li class="nav-item px-3">
            <a class="nav-link" href="{{ route('frontend.index') }}"><i class="icon-home"></i></a>
        </li>

        <li class="nav-item px-3">
            <a class="nav-link" href="{{ route('admin.dashboard') }}">{{ __('navs.frontend.dashboard') }}</a>
        </li>

        @if (config('locale.status') && count(config('locale.languages')) > 1)
            <li class="nav-item px-3 dropdown">
                <a class="nav-link dropdown-toggle nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                    <span class="d-md-down-none">{{ __('menus.language-picker.language') }} ({{ strtoupper(app()->getLocale()) }})</span>
                </a>

                @include('includes.partials.lang')
            </li>
        @endif
    </ul>

    <ul class="nav navbar-nav ml-auto">
        <li class="nav-item d-md-down-none">
            <a class="nav-link" href="#"><i class="icon-bell"></i><span class="badge badge-pill badge-info">0</span></a>
        </li>

        <li class="nav-item d-md-down-none">
            <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="true">
                <i class="icon-bell"></i><span class="badge badge-pill badge-danger">5</span>
            </a>
            {{--<div class="dropdown-menu dropdown-menu-right dropdown-menu-lg">
                <div class="dropdown-header text-center">
                    <strong>You have 5 notifications</strong>
                </div>
                <a href="#" class="dropdown-item">
                    <i class="icon-user-follow text-success"></i> New user registered
                </a>
                <a href="#" class="dropdown-item">
                    <i class="icon-user-unfollow text-danger"></i> User deleted
                </a>
                <a href="#" class="dropdown-item">
                    <i class="icon-chart text-info"></i> Sales report is ready
                </a>
                <a href="#" class="dropdown-item">
                    <i class="icon-basket-loaded text-primary"></i> New client
                </a>
                <a href="#" class="dropdown-item">
                    <i class="icon-speedometer text-warning"></i> Server overloaded
                </a>
                <div class="dropdown-header text-center">
                    <strong>Server</strong>
                </div>
                <a href="#" class="dropdown-item">
                    <div class="text-uppercase mb-1">
                        <small><b>CPU Usage</b></small>
                    </div>
                    <span class="progress progress-xs">
                        <div class="progress-bar bg-info" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                    </span>
                    <small class="text-muted">348 Processes. 1/4 Cores.</small>
                </a>
                <a href="#" class="dropdown-item">
                    <div class="text-uppercase mb-1">
                        <small><b>Memory Usage</b></small>
                    </div>
                    <span class="progress progress-xs">
                        <div class="progress-bar bg-warning" role="progressbar" style="width: 70%" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"></div>
                    </span>
                    <small class="text-muted">11444GB/16384MB</small>
                </a>
                <a href="#" class="dropdown-item">
                    <div class="text-uppercase mb-1">
                        <small><b>SSD 1 Usage</b></small>
                    </div>
                    <span class="progress progress-xs">
                        <div class="progress-bar bg-danger" role="progressbar" style="width: 95%" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100"></div>
                    </span>
                    <small class="text-muted">243GB/256GB</small>
                </a>
            </div>--}}
        </li>

        <li class="nav-item d-md-down-none">
            <a class="nav-link" href="#"><i class="icon-list"></i></a>
        </li>
        <li class="nav-item d-md-down-none">
            <a class="nav-link" href="#"><i class="icon-location-pin"></i></a>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                <img src="{{ $logged_in_user->picture }}" class="img-avatar" alt="{{ $logged_in_user->email }}">
                <span class="d-md-down-none">{{ $logged_in_user->full_name }}</span>
            </a>

            <div class="dropdown-menu dropdown-menu-right">
                <div class="dropdown-header text-center">
                    <strong>Heading</strong>
                </div>
                <a class="dropdown-item" href="#"><i class="fa fa-bell-o"></i> Link<span class="badge badge-info">0</span></a>
                <a class="dropdown-item" href="#"><i class="fa fa-envelope-o"></i> Link</a>

                <div class="dropdown-header text-center">
                    <strong>Heading</strong>
                </div>
                <a class="dropdown-item" href="#"><i class="fa fa-wrench"></i> Link</a>
                <a class="dropdown-item" href="#"><i class="fa fa-file"></i> Link<span class="badge badge-primary">0</span></a>
                <a class="dropdown-item" href="{{ route('frontend.auth.logout') }}"><i class="fa fa-lock"></i> {{ __('navs.general.logout') }}</a>
            </div>
        </li>
    </ul>

    <button class="navbar-toggler aside-menu-toggler" type="button">☰</button>
</header>