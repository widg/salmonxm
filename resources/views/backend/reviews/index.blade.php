@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('strings.backend.dashboard.title'))

@section('content')
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <strong>Каталог</strong> <a class="btn btn-outline-success btn-sm" href="product_add">Create</a>
                    <div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
                        <a href="{{ route('admin.product_add') }}" class="btn btn-success ml-1" data-toggle="tooltip" title="Create New"><i class="fa fa-plus-circle"></i></a>
                    </div><!--btn-toolbar-->
                </div><!--card-header-->
                <div class="card-block" id="app">
                    <table class="table table-sm">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Name</th>
                            <th scope="col">number</th>
                            <th scope="col">phone</th>
                            <th scope="col">comment</th>
                            <th scope="col">foto</th>
                            <th scope="col">checkbox_mail</th>
                            <th scope="col">checkbox_phone</th>
                        </tr>
                        </thead>
                        @if(isset($reviews))
                            <tbody>
                            @foreach ($reviews as $review)
                                <tr>
                                    <th scope="row">{{ $review->id }}</th>
                                    <td>{{ $review->name }}</td>
                                    <td>{{ $review->number }}</td>
                                    <td>{{ $review->phone }}</td>
                                    <td>{{ $review->comment }}</td>
                                    <td><a href="/images/guestbook/foto/{{ $review->foto }}">Фото</a></td>
                                    <td>{{ $review->checkbox_mail }}</td>
                                    <td>{{ $review->checkbox_phone }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        @else
                            <td>Нет отзывов</td>
                        @endif
                    </table>
                </div><!--card-block-->
            </div><!--card-->
        </div><!--col-->
    </div><!--row-->



@endsection