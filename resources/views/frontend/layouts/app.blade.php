<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="windows-1251">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">    
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="{{ URL::asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/all.css') }}">
    <script src="{{ URL::asset('js/mix.js') }}"></script>
    <script src="{{ asset('js/main.js') }}"></script>
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <script src="{{ asset('js/checkout.js') }}"></script>
    

    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <title>@yield('title')</title>

    <meta name="keywords" content="заказ еды дом ханты-мансийск круглосуточно доставка блюд ресторан " />
    <meta name="description" content="Служба доставки еды. У нас вы можете заказать блюда японской, мексиканской, французской и ирландской кухни. Наш телефон в Ханты-Мансийске: +7 (3467) 00-00-36" />

    <meta property="og:title" content="Лосьось — круглосуточная доставка еды в Ханты-Мансийске" />
    <meta property="og:description" content="Служба доставки еды . У нас вы можете заказать блюда японской, мексиканской, французской и ирландской кухни. Наш телефон в Ханты-Мансийске: +7 (3467) 00-00-36" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="/" />
    <meta property="og:image" content="" />
    <meta property="og:site_name" content="" />

    <script type="text/javascript">
        window.dataLayer = window.dataLayer || [];
    </script>

</head>

<body class="home ">
<style>
    .modal-backdrop{
        z-index: -1 !important;
    }
    .col-xs-1, .col-sm-1, .col-md-1, .col-lg-1, .col-xs-2, .col-sm-2, .col-md-2, .col-lg-2, .col-xs-3, .col-sm-3, .col-md-3, .col-lg-3, .col-xs-4, .col-sm-4, .col-md-4, .col-lg-4, .col-xs-5, .col-sm-5, .col-md-5, .col-lg-5, .col-xs-6, .col-sm-6, .col-md-6, .col-lg-6, .col-xs-7, .col-sm-7, .col-md-7, .col-lg-7, .col-xs-8, .col-sm-8, .col-md-8, .col-lg-8, .col-xs-9, .col-sm-9, .col-md-9, .col-lg-9, .col-xs-10, .col-sm-10, .col-md-10, .col-lg-10, .col-xs-11, .col-sm-11, .col-md-11, .col-lg-11, .col-xs-12, .col-sm-12, .col-md-12, .col-lg-12 {
    position: static !important;
    }
</style>
<div class="navbar-bg js-navbar-bg">
    <button class="navbar-bg__close"><i class="fa fa-angle-double-left" aria-hidden="true"></i></button>
</div>

<nav class="navbar-eda1 js-navbar">
    <div class="container">
        <div class="row">
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="navbar__collapse">
                <div class="navbar-eda1__city-check">
                
                    <div class="b-form-select-pad" id="cityByIp">
                        <div class="b-form-select js-select"><div class="b-form-select__active"><span class="js-current">Ханты-Мансийск</span></div><span class="b-form-select__drop-down"><span></span></span>
                            <ul class="b-form-select__options js-options" style="display: none;">
                                <li class="b-form-select__options__active-item js-current">Ханты-Мансийск</li>
                                <li class="b-form-select__options__item" redirect-url="/" base-city-url="/" style="display: none;">Ханты-Мансийск</li>
                                {{--
                                <li class="b-form-select__options__item" redirect-url="http://pervouralsk.eda1.ru/?change_city=1" base-city-url="http://pervouralsk.eda1.ru/">Сургут</li>
                                <!--li class="b-form-select__options__item" redirect-url="http://tagil.eda1.ru/?change_city=1" base-city-url="http://tagil.eda1.ru/">Нижний Тагил</li>
                                <li class="b-form-select__options__item" redirect-url="http://kamensk.eda1.ru/?change_city=1" base-city-url="http://kamensk.eda1.ru/">Каменск-Уральский</li>
                                <li class="b-form-select__options__item" redirect-url="http://chelyabinsk.eda1.ru/?change_city=1" base-city-url="http://chelyabinsk.eda1.ru/">Челябинск</li>
                                <li class="b-form-select__options__item" redirect-url="http://tyumen.eda1.ru/?change_city=1" base-city-url="http://tyumen.eda1.ru/">Тюмень</li>
                                <li class="b-form-select__options__item" redirect-url="https://www.eda1.ru/pyshma/?change_city=1" base-city-url="pyshma/index.html">Верхняя Пышма</li-->--}}
                            </ul>
                        </div>
                        <input class="js-input" type="hidden" value="/">
                    </div>


                    <script type="text/javascript">
                        $(function(){
                            $('#cityByIp').jsHtmlSelect({
                                token: ""
                            });
                         })
                    </script>


                </div>
                {{--
                <ul class="navbar__list_rest js-swipe-menu">
                                

                    <li class="navbar__list_rest__item "><a href="yaponskaya-kuhnya.html" class="navbar__list_rest__item__link js-list_rest__item__link">Сушкоф</a>

                        <ul class="navbar__list_rest__sub" style="display:none; " >

                            <li class=""><a href="yaponskaya-kuhnya/novinki.html">Новинки</a></li>
                            <li class=""><a href="pyshma/action.html" class="cat-page__menu__link">Филадельфия - Айс за 1 рубль</a></li>
                            <li class=""><a href="pyshma/dostavka-sushi.html">Сеты</a></li>
                            <li class=""><a href="pyshma/yaponskaya-kuhnya/rolly.html">Роллы</a></li>
                            <li class=""><a href="pyshma/yaponskaya-kuhnya/rolly-philadelphia.html">Роллы Филадельфия</a></li>
                            <li class=""><a href="pyshma/yaponskaya-kuhnya/gorjachie-rolly.html">Горячие роллы</a></li>
                            <li class=""><a href="pyshma/yaponskaya-kuhnya/prostye-rolly.html">Простые роллы</a></li>
                            <li class=""><a href="pyshma/yaponskaya-kuhnya/zapechennye-rolly.html">Запеченные роллы</a></li>
                            <li class=""><a href="pyshma/yaponskaya-kuhnya/sushi.html">Суши и закуски</a></li>
                            <li class=""><a href="pyshma/yaponskaya-kuhnya/yaponskie-salaty.html">Салаты</a></li>
                            <li class=""><a href="pyshma/yaponskaya-kuhnya/6/247.html">Супы</a></li>
                            <li class=""><a href="pyshma/yaponskaya-kuhnya/wok-menyu.html">Вок - меню</a></li>
                            <li class=""><a href="pyshma/yaponskaya-kuhnya/deserty-iz-sushkof.html">Десерты</a></li>
                            <li class=""><a href="pyshma/yaponskaya-kuhnya/napitki-iz-sushkof.html">Напитки</a></li>
                            <li class=""><a href="pyshma/yaponskaya-kuhnya/dopolnitelno.html">Дополнительно</a></li>
                        </ul>

                    </li>
                    <li class="navbar__list_rest__item "><a href="pyshma/picca.html" class="navbar__list_rest__item__link js-list_rest__item__link">Дель Песто</a>
                        <ul class="navbar__list_rest__sub" style="display:none; " >
                            <li class=""><a href="pyshma/picca/goryachee.html">Горячие блюда</a></li>
                            <li class=""><a href="pyshma/picca/picca-30-sm.html">Пицца 30 см</a></li>
                            <li class=""><a href="pyshma/picca/picca-40-sm.html">Пицца 40 см</a></li>
                            <li class=""><a href="pyshma/picca/deserty-iz-del-pesto.html">Десерты</a></li>
                            <li class=""><a href="pyshma/picca/napitki-iz-del-pesto.html">Напитки</a></li>
                        </ul>
                    </li>
                    <li class="navbar__list_rest__item "><a href="pyshma/lapsha.html" class="navbar__list_rest__item__link js-list_rest__item__link">Лапша тетушки Бунси</a>

                        <ul class="navbar__list_rest__sub" style="display:none; " >
                            <li class=""><a href="pyshma/lapsha/domashnyaya-lapsha.html">Домашняя лапша</a></li>
                            <li class=""><a href="pyshma/lapsha/napitki-ot-bunsi.html">Напитки</a></li>
                            <li class=""><a href="pyshma/lapsha/deserty-ot-bunsi.html">Десерты</a></li>
                        </ul>

                    </li>
                </ul>

                    --}}


                <ul class="navbar__list">
                    <li class="navbar__list__item_mob-show"><a href="/">Главная</a> </li>
                    <li class="navbar__list__item_mob-show navbar__list__item_foot-show"><a href="/conditions">Условия доставки</a> </li>
                    <li class="navbar__list__item_mob-show"><a href="/guestbook" >Оставить отзыв</a> </li>
                {{--<li class="navbar__list__item_foot-show"><a href="/new_vacancy">Вакансии</a> </li>--}}
                    <li class="navbar__list__item_mob-show"><a href="/bonus_card">Бонусная карта</a> </li>
                {{--<li class="navbar__list__item_mob-show"><a href="/">Контакты</a> </li>--}}

            </ul>




        </div><!-- /.navbar-collapse -->
    </div>
</div><!-- /.container-fluid -->
</nav>

<div class="header">
<div class="header__inner">
    <div class="container">
        <div class="row">
            <div class="header-menu" id="j_main">
               <div class="row">
                    <div class="col-xs-4 header-menu__item ">
                        <a href="/sushi" class="header-menu__item__link header-menu__item__link_3" ><span>Роллы</span></a>
                    </div>
                    <div class="col-xs-4 header-menu__item ">
                        <a href="/pizza" class="header-menu__item__link header-menu__item__link_6" ><span>Пицца</span></a>
                    </div>
                    {{--
                    <div class="col-xs-4 header-menu__item ">
                        <a href="/lapsha" class="header-menu__item__link header-menu__item__link_11" ><span>Лапша</span></a>
                    </div>--}}
                    </div>
                </div>

                <script type="text/javascript">
                    $(document).ready(function() {
                        var restaurant_id = ''; //3
                        $('#j_main').addClass('header-menu_' + restaurant_id);
                });
                </script>   

                <div class="header-contact hidden-xs hidden-sm hidden-md">
                    <div class="row">
                        <div class="col-xs-6 col-sm-4 text-center">
                            <div class="header-contact__phones-o">
                                <div class="header-contact__phones">
                                    <div class="header-contact__phones__item"><span class="header-contact__phones__item__title">&nbsp;</span> <span class="callibri_che_01">Номер телефона для заказа +7 952 708 00 00</span></div>
                                </div>{{--
                                <div class="col-sm-8 header-contact__time-info">
                                    <div class="header-contact__time">
                                        10:00-23:00 ежедневно
                                    </div>
                                </div>--}}
                            </div>
                        </div>
						
                        <div class="col-xs-6 col-sm-4 text-center">
                            <div class="header-contact__phones-o">
                                <div class="header-contact__phones">
                                    <div class="header-contact__phones__item"><span class="callibri_che_03"><span class="header-contact__phones__item__title">{{--Горячая линия:--}}</span> 10:00-23:00 ежедневно</span></div>{{--
                                    <div class="header-contact__nocall">
                                        <a href="#" id="nocall-button" data-toggle="modal" data-target="#js-nocall-popup">Заказать звонок</a>
                                    </div>--}}
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-4 text-center">

                        {{--<ul class="nav navbar-nav navbar-right">
                            @if (auth()->guest())
                                <li><a href="#" data-toggle="modal" data-target="#login-modal">
                                <i class="fa fa-user fa-btn"></i>&nbsp;Войти</a></li>
                                <li><a href="#" data-toggle="modal" data-target="#register-modal">
                                <i class="fa fa-sign-in fa-btn"></i>&nbsp;Регистрация</a></li>
                            @else
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" id="navbar-profile" data-toggle="dropdown" role="button" aria-expanded="false">
                                        <!--img src="{{ asset('images/uploads/avatars') }}/{{ auth()->user()->avatar }}" id="navbar-profile-avatar" alt="Пользователь {{  auth()->user()->name }}">-->
                                        &nbsp;{{  auth()->user()->name }} <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="/user/profile"><i class="fa fa-user fa-btn"></i>&nbsp;Профиль</a></li>
                                        <li><a href="/logout"><i class="fa fa-sign-out fa-btn"></i>&nbsp;Выйти</a></li>
                                    </ul>
                                </li>
                            @endif
                        </ul>--}}
                        {{--
                            <div class="header-contact__phones-o">
                                <div class="header-contact__phones">
                                    <div class="header-contact__phones__item"><span class="header-contact__phones__item__title">&nbsp;</span> <span class="callibri_che_02">+7 904 471 32 72</span></div>
                                </div>
                            </div>--}}
                        </div>

                    </div>
                    {{--
                    <div class="row">
                        <div class="col-sm-8 header-contact__time-info">
                            <div class="header-contact__time">
                                Круглосуточно
                            </div>
                        </div>
                        <div class="col-lg-9">
                            <div class="header-contact__phones__callback"><a href="">Обратный звонок</a></div>
                        </div>
                    </div>--}}
                </div>
            </div>
        </div>
    </div>
</div>
<div class="header-basket">
    <div class="container">
        <div class="row">
            <button class="header__bars js-header-bars">
                <i class="fa fa-bars" aria-hidden="true"></i>
            </button>
            
                <script type="text/javascript">
                    var WindowHeight = $( window ).height();

                    $(".js-header-bars").click(function() {
                        $('.js-navbar-bg').show();
                        $('.js-navbar').show();
                        setTimeout(function(){
                            $('.navbar-bg').addClass('navbar-bg__open');
                        }, 100);
                        setTimeout(function(){
                            $('.navbar__collapse').addClass('navbar__collapse__open');
                        }, 100);
                        setTimeout(function(){
                            $('.navbar__collapse').addClass('navbar__collapse__opened');
                        }, 200);
                        $('html').css({"height": WindowHeight,'overflow-y':'hidden'});
                        $('body').css({"height": WindowHeight,'overflow-y':'hidden'});
                        $('.page').addClass('js-navbar-bg');
                    });
                    $(".js-navbar-bg").click(function() {
                        $('.navbar__collapse').removeClass('navbar__collapse__opened');
                        $('.navbar-bg').removeClass('navbar-bg__open');
                        $('.navbar__collapse').removeClass('navbar__collapse__open');
                        setTimeout(function(){
                            $('.js-navbar-bg').hide();
                            $('.js-navbar').hide();
                        }, 200);
                        $('html').css({"height": 'auto','overflow-y':'auto'});
                        $('body').css({"height": 'auto','overflow-y':'auto'});
                        $('.page').removeClass('js-navbar-bg');
                    });
                </script>
            
            <div class="text-center header__basket">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="b-basket-2">
                            <div class="order_basket al" id="basket_cont">
                                <a href="#basket"  class="b-basket__open-popup" id="basket_fly">
                                    <span class="b-basket__open-popup__title">Ваш заказ</span>
                                    <span class="b-basket__open-popup__price">0 <i class="fa fa-rub" aria-hidden="true"></i></span>
                                    <span class="b-basket__open-popup__count"></span>&nbsp;     </a>
                                <!--img src="/images/ico-basket.gif" alt="" title="" class="ico-basket" /
                                <a href="#" onclick="if(!$.cookie('step_order') || $.cookie('step_order') == 5){$.cookie('step_order', 1, {expires: 0,  path:  '/'});sendYandexMetric('go-to-basket');}  createOrder(); return false;" class="b-basket__open-popup  " id="basket_fly">-->

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="header__basket__call">
                <a href="tel:+7 (952) 708-00-00"></a>
            </div>

        </div>
    </div>
</div>

@yield('content')

@include('frontend.modals.loginModal')
@include('frontend.modals.registerModal')

<div class="footer-menu">
    <div class="container">
        <ul class="footer-menu__list">
            <li class="navbar__list__item_mob-show"><a href="/">Главная</a> </li>
            {{--<li class="navbar__list__item_foot-show"><a href="/new_vacancy">Вакансии</a> </li>--}}
			<li class="navbar__list__item_mob-show"><a href="/bonus_card">Бонусная карта</a> </li>
            <li class="navbar__list__item_mob-show"><a href="/guestbook" >Оставить отзыв</a> </li>
            <li class="navbar__list__item_mob-show navbar__list__item_foot-show"><a href="/conditions">Условия доставки</a> </li>
            {{--<li class="navbar__list__item_mob-show"><a href="/">Контакты</a> </li>--}}
			<li class="footer-menu__last-empty">&nbsp;</li>
        </ul>
    </div>
</div>
<footer class="footer" itemscope itemtype="http://schema.org/Organization">
    <div class="container">
        <div class="row">
            <div class="col-md-5 col-lg-3 hidden-xs hidden-sm">
                <div class="footer__cont">
                    <div class="footer__copyright">
                        ©2016-2017 <span itemprop="name">Лосось</span>
                        <br>
                        Все торговые знаки зарегистрированы. Город Ханты-Мансийск, 2018 г.<br/>
                        Использование сайта, означает согласие с <a href="/terms-use">пользовательским соглашением.</a>
                    </div>
                    {{--
                    <div class="footer__map"><a href="sitemap.html">Карта сайта</a></div>
                    <div class="footer__map"><a href="#" class="footer__mobile-ver js-site-desktop">Мобильная версия</a></div>--}}
                </div>
            </div>
            <div class="col-md-7 col-lg-6">
                <div class="footer__contact-cont">
                    <div class="footer__adr hidden-xs" itemscope="itemscope" itemtype="http://schema.org/PostalAddress">Наш адрес: г. Ханты-Мансийск, ул. Сирина, д. 78 
                        <br />Телефон: +7 (952) 708-00-00
                    </div>
					{{--
                    <div class="row">
                        <div class="col-md-5 footer__phone" itemprop="telephone">
                            <span class="callibri_che_02">+7 000 000 00 00</span>
                        </div>
                        <div class="col-md-7 footer__hot" itemprop="telephone">
                            <span class="callibri_che_03"><span class="footer__hot__title">Горячая линия:</span> +7 800 000 00 00</span>
                        </div>
                        <div class="col-md-5 footer__phone" itemprop="telephone">
                            <span class="callibri_che_01">+7 000 000 00 00</span>
                        </div>
                        <div class="col-md-7 footer__callback">
                            <span><a href="#" id="nocall-button" data-toggle="modal" data-target="#js-nocall-popup">Заказать звонок</a></span>
                        </div>
                    </div>--}}
                </div>
            </div>
            <div class="col-md-12 col-lg-3">
                <div class="footer__soc-cont">
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            <ul class="footer__soc">
							{{--
                                <li class="footer__soc__item"><a href="http://www.facebook.com/0000" target="_blank" class="footer__soc__link footer__soc__link_f"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                <li class="footer__soc__item"><a href="https://twitter.com/0000" target="_blank" class="footer__soc__link footer__soc__link_t"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>--}}
                                <li class="footer__soc__item"><a href="http://instagram.com/salmonhm" target="_blank" class="footer__soc__link footer__soc__link_i"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                <li class="footer__soc__item"><a href="http://vk.com/salmonxm" target="_blank" class="footer__soc__link footer__soc__link_f"><i class="fa fa-vk" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                        <div class="col-lg-12 footer__map"><a href="#" class="footer__desktop-ver js-site-desktop">Полная версия</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>



<table id="js-order-popup-error-message" class="authorize-cont" style="display:none;" >
    <tr>
        <td>
            <div class="authorize-block">
                <form action="#" method="post" >
                    <div class="popup-message"></div>
                    <div class="ac bottom"><input type="button" value="" class="close button" /></div>
                </form>
                <span class="popup-close"><img src="{{URL::asset('images/close--.gif')}}" alt="" title="" /></span>
            </div>
        </td>
    </tr>
</table>

<div class="b-popup-shadow js-popup-shadow"></div>

</body>
</html>