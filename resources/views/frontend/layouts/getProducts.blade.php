<div class="container-fluid">
    <div class="row">
        <div class="col-md-8">
            @if (Session::has('success_message'))
                <div class="alert alert-dismissable alert-success">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    <p class="text-center">{{ Session::get('success_message') }}</p>
                </div>
            @endif
            @if (Session::has('error'))
                <div class="alert alert-dismissable alert-danger">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    <p class="text-center">{{ Session::get('error') }}</p>
                </div>
            @endif
            @foreach($products as $product)

                    <div class="col-sm-6 col-md-4 col-lg-4 product-container" data-order_count=0 data-price=236 >
                        <div itemscope itemtype="http://schema.org/Product" class="cat-page__item visible_delivery visible_self" id="product_2508"> 
                            <div class="rows-product js-cat-page__item">
                                <div class="cat-page__item__img">
                                    <a href="#" class="popup-img" data-url="/upload/images/16a5b.jpg" data-toggle="modal" data-target="#js-new-image">
                                    <img src="{{ URL::asset("/{$product->pathToImage}") }}" itemprop="image" alt="{{ $product->name }}" title="{{ $product->name }}" class="id_2508"/>
                                    </a>
                                </div>

                                <div class="cat-page__item__desc js-page__item__desc">
                                    <div class="cat-page__item__desc__open"><i class="fa fa-info" aria-hidden="true"></i></div>

                                    <div class="cat-page__item__title"><a itemprop="url" href="pyshma/yaponskaya-kuhnya/novinki/2508.html"><span itemprop="name" class="name"> {{ $product->name }} </span></a></div>
                                    <div class="cat-page__item__info" itemprop="description">
                                        {!! $product->composition !!}
                                    </div>
                                    <div class="cat-page__item__weight">
                                        <span>{{ $product->weight }} г</span>
                                    </div>

                                    <script type="text/javascript">
                                            /*$( ".cat-page__item__desc" ).click(function() {
                                                if ($( this ).hasClass( "cat-page__item__desc_all" )){
                                                    $( this ).removeClass('cat-page__item__desc_all');
                                                }
                                            });*/
                                            $( ".cat-page__item__desc__open" ).mouseenter(function() {
                                                $( this ).closest( ".cat-page__item__desc" ).addClass('cat-page__item__desc_all');
                                              });
                                            $( ".cat-page__item__desc" ).mouseleave(function() {
                                                $( this ).removeClass('cat-page__item__desc_all');
                                              });
                                    </script>

                                    <div class="cat-page__item__actions">
                                        <a href="#" class="btn btn-success btn-sm cat-page__item__add-card show_2508 id_2508 plus" style="display: none;" onclick="sendYandexMetric('Add-to-basket');basket.change(2508, 'add',0); return false;">+1</a>
                                        <a href="#" class="btn btn-primary btn-sm cat-page__item__add-card show_2508 minus" style="display: none;" onclick="basket.change(2508, 'sub',0);return false;">-1</a>
                                        {{--<a href="{{ url("/{$product->category}", $product->id) }}" class="btn btn-primary btn-sm cat-page__item__add-card hide_2508 id_2508"   onclick="sendYandexMetric('Add-to-basket');basket.change(2508, 'add',0);return false;">В корзину</a>--}}
                                        <a href="" product-id="{{ $product->id }}" class="btn btn-primary btn-sm cat-page__item__add-card hide_2508 id_2508 add-to-cart"><i class="fa fa-shopping-cart"></i>&nbsp;В корзину</a>
                                    </div>
                                
                                    <div class="cat-page__item__price ">
                                        <span itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                                        <span class="price_delivery" itemprop="price"> {{ $product->price }} </span> <span class="cat-page__item__currency"><i class="fa fa-rub" aria-hidden="true"></i></span>
                                        <meta itemprop="priceCurrency" content="RUB">
                                        <link itemprop="availability" href="http://schema.org/InStock">
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>





                {{--<div class="col-md-4">
                    <div class="thumbnail">
                        <a href="{{ url("/{$product->category}", $product->id) }}">
                            <img src="{{ URL::asset("/images/{$product->image}") }}"
                                 alt="{{ $product->name }}" title="{{ $product->name }}"
                                 style="width: 288px; height: 161px;" class="img-responsive">
                        </a>
                        <div class="caption">
                            <h3 class="pull-right"><span class="label label-info">{{ $product->price }} руб.</span></h3>
                            <h3 id="product_name">{{ $product->name }}</h3>
                            <p id="product_composition">{{ $product->composition }}</p>
                            <a href="" product-id="{{ $product->id }}" class="btn btn-success add-to-cart"><i class="fa fa-shopping-cart"></i>&nbsp;В корзину</a>
                            <a href="{{ url("/{$product->category}", $product->id) }}" class="btn btn-primary pull-right">
                                Подробнее
                                <i class="fa fa-arrow-right"></i>
                            </a>
                        </div>
                    </div>
                </div>--}}
            @endforeach
        </div>
        <div class="col-md-3 sidebar">
            @include('frontend.cart.cart')
        </div>  
    </div>
</div>