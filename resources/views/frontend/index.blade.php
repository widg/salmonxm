@extends('frontend.layouts.app')

@section('title', 'SALMONXM.RU')

@section('content')
{{-- тут --}}
<script type="text/javascript">
    $(function(){
        $('#citySelect').jsHtmlSelect();
//      $('#city_select_popup').show');
        $(document).on('click', '#js-set-city', function(){
            if($('#citySelect').is(':visible')){
                location.href = $('#citySelect .js-input').val();
            } else {
                //alert(location.hostname);
                $('#js-city_select_popup').hide();
            }
        });

        $.cookie('confirmCity', 0, {expires: 365,  path:  '/', domain: getMainHost()});

    })
</script>

<script type="text/javascript">
    var size_global=6;
    var g_currentTimeH=11;
    var g_currentTimeM=03;
    var g_currentTimeS=19;
</script>

<div class="home-img-banner">
        <div class="home-img-banner__item home-img-banner__item_left-bottom">
            <a href="https://vk.com/salmonxm">
                <img src="images/new/Pizza-1066-.jpg" alt="" class="home-img-banner__small" />
                <span class="home-img-banner__small-true" data-trueimage="/images/new/banner1.png"></span>
                <span class="home-img-banner__big" data-trueimage="/images/new/banner1.png" style="background-image: url(images/new/Pizza-1920-.jpg);"></span>
                <span class="home-img-banner__big-true"></span>
            </a>
        </div>
        <div class="home-img-banner__item home-img-banner__item_left-top">
            <a href="sushi">
                <img src="images/new/promo-1rub-min-.jpg" alt="" class="home-img-banner__small" />
                <span class="home-img-banner__small-true" data-trueimage="images/new/banner2.png"></span>
                <span class="home-img-banner__big" data-trueimage="/images/new/banner2.png" style="background-image: url(images/new/promo-1rub-big-.jpg);"></span>
                <span class="home-img-banner__big-true"></span>
            </a>
        </div>

</div>
    <script type="text/javascript">
        function loadImageDfd(src) {
            var dfd = $.Deferred();
        
            var loadImg = new Image();
            loadImg.onload = function() {
                dfd.resolve();
            };
            loadImg.src = src;
            
            return dfd;
        }
        
        function loadImagesArray(images, callback) {
            var deffereds = [];
            for(var i=0; i<images.length; i++) {
                deffereds.push(loadImageDfd(images[i]));            
            }
            $.when.apply($, deffereds).done(callback);
        }
    
        $(function(){
            
            var images = [];

            var images1 = ['/images/new/header-menu_sushkof-mob.png','images/new/header-menu_delpesto-mob.png','/images/new/header-menu_bunsi-mob.png'];

            loadImagesArray(images1, function() {
            });
            
            $( ".home-img-banner__big,.home-img-banner__small-true" ).each(function( index ) {
                images[index] = $(this).attr('data-trueimage');
            });
            
            loadImagesArray(images, function() {
                imgLoaded(images);
            });
            
            function imgLoaded(images){
                $( ".home-img-banner__big" ).each(function( index ) {
                    $(this).next('.home-img-banner__big-true').css({
                        "background-image": "url(" + $(this).attr('data-trueimage') + ")"
                    });
                });
                $('.home-img-banner__big-true').addClass('home-img-banner__big-true_show');
                $( ".home-img-banner__small-true" ).each(function( index ) {
                    $(this).css({
                        "background-image": "url(" + $(this).attr('data-trueimage') + ")"
                    });
                });
                $('.home-img-banner__small-true').addClass('home-img-banner__small-true_show');
                setTimeout( function() {
                    $('.home-img-banner').owlCarousel({
                        loop:true,
                        margin:0,
                        autoplay:true,
                        smartSpeed: 1000,
                        autoplayTimeout:5000,
                        autoplayHoverPause:false,
                        nav:true,
                        responsive:{
                            0:{
                                items:1
                            }
                        }
                    }); 
                },700);
            };
        });
    </script>

<div class="main-bg">
    <div class="categories-list">
        <div class="container">
            <div class="categories-list__title">{{--Свежая и разнообразная кухня--}}</div>
            <div class="row">
                <div class="col-xs-6 col-lg-3"><div class="categories-list__item"><a href="sushi" class="categories-list__item__link"><img src="images/new/categorie
				s-list__item-sushi.png" class="categories-list__item__img"><span class="categories-list__item__name">Суши</span></a></div></div>
                <div class="col-xs-6 col-lg-3"><div class="categories-list__item"><a href="sushi" class="categories-list__item__link"><img src="images/new/categories-list__item-roll.png" class="categories-list__item__img"><span class="categories-list__item__name">Роллы</span></a></div></div>
                <div class="col-xs-6 col-lg-3"><div class="categories-list__item"><a href="sushi" class="categories-list__item__link"><img src="images/new/categories-list__item-hotroll.png" class="categories-list__item__img"><span class="categories-list__item__name">Горячие роллы</span></a></div></div>
                <div class="col-xs-6 col-lg-3"><div class="categories-list__item"><a href="sushi" class="categories-list__item__link"><img src="images/new/categories-list__item-set.png" class="categories-list__item__img"><span class="categories-list__item__name">Сеты</span></a></div></div>
                <div class="col-xs-6 col-lg-3"><div class="categories-list__item"><a href="sushi" class="categories-list__item__link"><img src="images/new/categories-list__item-philka.png" class="categories-list__item__img"><span class="categories-list__item__name">Запечённые роллы</span></a></div></div>
                <div class="col-xs-6 col-lg-3"><div class="categories-list__item"><a href="pizza" class="categories-list__item__link"><img src="images/new/categories-list__item-pizza.png" class="categories-list__item__img"><span class="categories-list__item__name">Пицца</span></a></div></div>
				{{--
                <div class="col-xs-6 col-lg-3"><div class="categories-list__item"><a href="pyshma/lapsha.html" class="categories-list__item__link"><img src="images/new/categories-list__item-lapsha.png" class="categories-list__item__img"><span class="categories-list__item__name">Лапша</span></a></div></div>
                <div class="col-xs-6 col-lg-3"><div class="categories-list__item"><a href="pyshma/yaponskaya-kuhnya/wok-menyu.html" class="categories-list__item__link"><img src="images/new/categories-list__item-tyahan.png" class="categories-list__item__img"><span class="categories-list__item__name">Тяханы</span></a></div></div>--}}
            </div>
        </div>
    </div>
        <script type="text/javascript">
            $(function(){
                if($(window).width()<992){
                    $('#news-slider').owlCarousel({
                        center: true,
                        items:2,
                        loop:true,
                        margin:10,
                        nav:false,
                        dots:false
                    });
                } else {
                    $('#news-slider').owlCarousel({
                        center: false,
                        items:2,
                        loop:true,
                        margin:34,
                        nav:true,
                        dots:false
                    });
                }       
            });
        </script>
    


    
    <script type="text/javascript">
        var is_off_order=0;
    </script>
    
        <script type="text/javascript">
            $(function(){

                 initCarousel();

                function initCarousel(){
                    if($(window).width()<768){
                        $('#guest-last-choice-slider').owlCarousel({
                            center: true,
                            items:2,
                            loop:false,
                            margin:15,
                            nav:false,
                            dots:false
                        });
                    } else if($(window).width()<992){
                        $('#guest-last-choice-slider').owlCarousel({
                            center: true,
                            items:3,
                            loop:false,
                            margin:34,
                            nav:false,
                            dots:false
                        });
                    } else {
                        $('#guest-last-choice-slider').owlCarousel({
                            center: false,
                            items:4,
                            loop:false,
                            margin:22,
                            nav:true,
                            dots:false
                        });
                    }
                }
            });
        </script>
    
    
    
        <script type="text/javascript">
            $(function(){
                if($(window).width()<992){
                    $('#flamp-slider').owlCarousel({
                        center: true,
                        items:2,
                        loop:true,
                        margin:34,
                        nav:false,
                        dots:false
                    });         
                } else {
                    $('#flamp-slider').owlCarousel({
                        center: false,
                        items:3,
                        loop:true,
                        margin:34,
                        nav:true,
                        dots:false
                    });         
                }       
            });
        </script>
    

</div>

{{-- тут --}}
@endsection