@extends('frontend.layouts.catalog')

@section('title', 'Сайт Заказа Пиццы')

@section('content')
{{-- тут --}}
<div class="container">
    <div class="row">

    </div>
    <div class="product-page__item content-block" id="js-prod-data-3794" >
        <div class="row">
            <div class="col-lg-5">
                <div class=" product-page__img">
                    <div class="product-page__img__item">
                        <img src="/images/new/shipping_fast.png" alt="2 пиццы за 899 р. и подарок!" title="">
                    </div>
                </div>
            </div>
            <div class="col-lg-7">
                <h1 class="product-page__title">Условия доставки</h1>

                <div class="product-page__weight__nutritional">
                </div>
                <div class="product-page__desc">
                    <p>1. Минимальная стоимость заказа 300 рублей.</p>
                    <p>2. При заказе до 600 рублей, доставка является платной.</p>
                    <p>Стоимость платной доставки составляет 50 рублей.</p>
                    <p>Заказы с выше 600 рублей бесплатные, за исключением доставки в отдаленные районы города, а именно:</p>
                    <p>ОМК, Учхоз, район аэропорта, Су-967.</p>
                    <p>В случае, если сумма заказа менее 600 рублей, и доставка осуществляется в отдаленный район,
                        стоимость доставки составляет 100 рублей.</p>
                    <p>3. Среднее время доставки заказа составляет 1 час.</p>
                    <p>При большой нагрузке на кухню время ожидания заказа может быть увеличено
                        по заявлению оператора.</p>
                    <p>4. Так же вы можете оформиф заказ забрать его самостоятельно по адресу: г. Ханты-Мансийск, ул. Сирина, дом 78.</p>
                    <br>
                </div>

            </div>
        </div>
    </div>
</div>
{{-- тут --}}

@endsection