@extends('modals.modalLayout', [
    'modalName' => 'callme-modal',
    'modalTitle' => 'Вход',
    'formAction' => '/callme',
    'formID' => 'callmeForm',
    'formName' => 'callmeForm',
    'modalButtonTitle' => 'Войти'
])

@section('formContent')
    <div class="form-group" id="name-div">
        <label class="control-label" for="name">Имя</label>
        <input id="name" type="name" placeholder="example@gmail.com" title="Пожалуйста, введите своё имя" required value="" name="name" class="form-control">
        {{-- <div id="form-errors-name" class="has-error"></div> --}}
        <span class="help-block">
            <strong id="form-errors-name"></strong>
        </span>
        <span class="help-block small">Ваше имя</span>
    </div>
    <div class="form-group" id="phome-div">
        <label class="control-label" for="phome">Телефон</label>
        <input type="phome" title="Пожалуйста, введите ваш пароль" placeholder="8999887766" required value="" name="phome" id="phome" class="form-control">
        <span class="help-block">
            <strong id="form-errors-phome"></strong>
        </span>
        <span class="help-block small">Ваш пароль</span>
    </div>
    <div class="form-group" id="login-errors">
        <span class="help-block">
            <strong id="form-login-errors"></strong>
        </span>
    </div>
    <div class="form-group">
        <div class="checkbox">
            <label>
                <input type="checkbox" name="remember"> Запомнить меня
            </label>
        </div>
    </div>
@overwrite


