@extends('frontend.modals.modalLayout', [
    'modalName' => 'payment-modal',
    'modalTitle' => 'Подтверждение заказа',
    'formAction' => '/user/buy',
    'formID' => 'payment-form',
    'formName' => 'payment-form',
    'modalButtonTitle' => 'Заказать'
])

@section('formContent')
    <div class="panel panel-default">
        <div class="form-group" id="register-name">
            <label class="control-label" for="name">Имя</label>
            <input class="form-control" id="name" name="name" placeholder="Иван Иванов"  title="Пожалуйста, введите своё имя" type="text"
                   value="{{--@if (Auth::check()){{ $user->name }}@endif--}}">
            <span class="help-block"><strong id="register-errors-name"></strong></span> <span class="help-block small">
        </span>
        </div>
        <div class="form-group" id="register-email">
            <label class="control-label" for="email">Email</label>
            <input class="form-control" id="email" name="email" placeholder="example@mail.com" title="Пожалуйста, введите свой email" type="email"
                   value="{{--@if (Auth::check()){{ $user->email }}@endif--}}">
            <span class="help-block"><strong id="register-errors-email"></strong></span> <span class="help-block small">
        </span>
        </div>
        <div class="form-group" id="register-phone_number">
            <label class="control-label" for="phone_number">Телефон<span class="control-label__important">*</span></label>
            <input class="form-control" id="phone" name="phone" placeholder="+79999999999" required="" title="Пожалуйста, введите свой номер телефона" type="text"
                   value="{{--@if (Auth::check()){{ $user->phone_number }}@endif--}}">
            <span class="help-block"><strong id="register-errors-phone_number"></strong></span> <span class="help-block small">
        </span>
        </div>
        <div class="form-group" id="register-card_number">
            <label class="control-label" for="card_number">Карта постоянного клиента</label>
            <input class="form-control" id="card" name="card" placeholder="999" title="Пожалуйста, введите три последнии цифры карты" type="text"
                   value="{{--@if (Auth::check()){{ $user->phone_number }}@endif--}}">
            <span class="help-block"><strong id="register-errors-card_number"></strong></span> <span class="help-block small">
        </span>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-6 col-lg-4">
                <div class="form-group">
                    <label class="control-label">Улица <span class="control-label__important">*</span></label>
                    <input name="street" id="street_auto" type="text" class="form-control ui-autocomplete-input" value="" >
                    <input type="hidden" name="address[street]" class="order_data" value="">
                </div>
            </div>
            <div class="col-xs-3 col-md-1 col-lg-2">
                <div class="form-group">
                    <label class="control-label">Дом <span class="control-label__important">*</span></label>
                    <input type="text" class="form-control order_data" name="home">
                </div>
            </div>
            <div class="col-xs-3 col-md-1 col-lg-2">
                <div class="form-group">
                    <label class="control-label">Кв</label>
                    <input type="text" class="form-control order_data" name="apart">
                </div>
            </div>

            <div class="col-xs-3 col-md-1 col-lg-2">
                <div class="form-group">
                    <label class="control-label">Подъезд</label>
                    <input type="text" class="form-control order_data" name="pod">
                </div>
            </div>
            <div class="col-xs-3 col-md-1 col-lg-2">
                <div class="form-group">
                    <label class="control-label">Этаж</label>
                    <input type="text" class="form-control order_data" name="et">
                </div>
            </div>
        </div>
        <div class="form-group" id="register-descr">
            <label class="control-label" for="descr">Комментарий</label>
            <input class="form-control" id="descr" name="descr"  type="text"
                   value="{{--@if (Auth::check()){{ $user->phone_number }}@endif--}}">
            <span class="help-block"><strong id="register-errors-descr"></strong></span> <span class="help-block small">
        </span>
        </div>
        <div class="form-group" id="register-person">
            <label class="control-label" for="person">Колличество персон</label>
            <input class="form-control" id="person" name="person" placeholder="01"  title="Пожалуйста, введите колличество персон" type="text"
                   value="{{--@if (Auth::check()){{ $user->phone_number }}@endif--}}">
            <span class="help-block"><strong id="register-errors-person"></strong></span> <span class="help-block small">
        </span>
        </div>
        {{--<div class="form-group" id="register-address">
            <label class="control-label" for="address">Адрес</label>
            <input class="form-control" id="address" name="address" placeholder="ул.Пушкина, д.2, к.1, кв.17" title="Пожалуйста, введите свой адрес" type="address"
                   value="{{--@if (Auth::check()){{ $user->address }}@endif">
            <span class="help-block"><strong id="register-errors-address"></strong></span> <span class="help-block small">
        </span>
        </div>--}}
        <label for="confirmation" class="cr js-cr is-checkbox confirmation on">
            <input type="checkbox" name="confirmation" id="confirmation" value="Y">
            Я подверждаю свое согласие с условиями <a href="terms-use" target="_blank">пользовательского соглашения</a>
        </label>
    </div>
@endsection