@extends('frontend.layouts.catalog')

@section('title', 'Сайт Заказа Пиццы')

@section('content')
<div class="container">
    <div class="row">

    </div>
    <div class="product-page__item content-block" id="js-prod-data-3794" data-id="3794" data-accid="2717" data-name="2 пиццы за 899 р. и подарок!" data-price="899" data-category="Новые пиццы за 899 рублей!" data-brand="Дель Песто" data-photo="Array">
        <div class="row">
            <div class="col-lg-5">
                <div class=" product-page__img">
                    <div class="product-page__img__item">
                        <img src="/images/new/bumag86.png" alt="" title="">
                    </div>
                </div>
            </div>
            <div class="col-lg-7">
                <h1 class="product-page__title">Оставить отзыв</h1>

                <div class="product-page__weight__nutritional">
                </div>
                <div class="product-page__desc">
                    @if (Session::has('success_message'))
                        <div class="alert alert-dismissable alert-success">
                            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                            <p class="text-center">{{ Session::get('success_message') }}</p>
                        </div>
                    @endif
                    <h4>В этом разделе вы можете оставить ваш отзыв, заполнив предоставленную ниже форму.</h4>
                    <form action="guestbook" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group" id="edit-name">
                            <label class="control-label" for="name">Имя</label>
                            <input class="form-control" id="name" name="name" required="" title="Пожалуйста, введите своё имя" type="text" value="">
                            <span class="help-block"><strong id="edit-errors-name"></strong></span>
                        </div>
                        <div class="form-group" id="edit-phone_number">
                            <label class="control-label" for="number">№ заказа</label>
                            <input class="form-control" id="number" name="number" required="" title="Пожалуйста, введите свой номер телефона" type="text" value="">
                            <span class="help-block"><strong id="edit-errors-phone_number"></strong></span>
                        </div>
                        <div class="form-group" id="phone">
                            <label class="control-label" for="phone">№ телефона</label>
                            <input class="form-control" id="phone" name="phone" required="" title="Пожалуйста, введите свой email" type="phone" value="">
                            <span class="help-block"><strong id="edit-errors-email"></strong></span>
                        </div>
                        <div class="form-group" id="edit-address">
                            <label class="control-label" for="address">Комментарий</label>
                            <input class="form-control" id="address" name="comment" required="" title="Пожалуйста, введите улицу" type="street" value="">
                            <span class="help-block"><strong id="edit-errors-address"></strong></span>
                        </div>
                        <label class="control-label" for="address">Фото заказа</label>
                        <div class="input-group" id="avatar-upload">

                            <label class="input-group-btn">
                    <span class="btn btn-primary">
                        Browse&hellip; <input type="file" style="display: none;" multiple name="foto">
                    </span>
                            </label>
                            <input type="text" class="form-control" id="image-name" readonly>
                        </div>
                        <br>
                        <label class="control-label" for="address">Каким способом вам ответить</label>
                        <div>
                            <label class="checkbox-inline"><input name="checkbox_mail" type="checkbox" value="1">по емайлу</label>
                            <label class="checkbox-inline"><input name="checkbox_phone" type="checkbox" value="1">по телефону</label>
                        </div>
                        <br>

                        <button class="btn btn-success">Отправить</button>

                    </form>
                    {{--1 ИО 2 № заказа 3 фото заказа 4 № телефона 5 комментарий 6 каким способом вам ответить (майл/по телефону)--}}

                    <br>
                    <p>Спасибо Вам за отзыв!</p>

                    <p>Каждый отзыв важен для нас!</p>

                    <p>Мы работаем над улучшением качества обслуживания наших клиентов.</p>
                    <br>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection