<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="windows-1251">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">    
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="{{ URL::asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/all.css') }}">
    <script src="{{ URL::asset('js/app.js') }}"></script>
    <script src="{{ asset('js/main.js') }}"></script>
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <script src="{{ asset('js/checkout.js') }}"></script>
    
    <link rel="apple-touch-icon" sizes="57x57" href="apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favicon-16x16.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <title>@yield('title')</title>

    <meta name="keywords" content="заказ еды дом ханты-мансийск круглосуточно доставка блюд ресторан " />
    <meta name="description" content="Служба доставки еды. У нас вы можете заказать блюда японской, мексиканской, французской и ирландской кухни. Наш телефон в Ханты-Мансийске: +7 (3467) 36-25-36" />

    <meta property="og:title" content="Сушкоф — круглосуточная доставка еды в Ханты-Мансийске" />
    <meta property="og:description" content="Служба доставки еды EDA1.RU. У нас вы можете заказать блюда японской, мексиканской, французской и ирландской кухни. Наш телефон в Ханты-Мансийске: +7 (3467) 36-25-36" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="/" />
    <meta property="og:image" content="" />
    <meta property="og:site_name" content="" />


    <script type="text/javascript">
        window.dataLayer = window.dataLayer || [];
    </script>

</head>

<body class="home ">

<div style="display:none;">

</div>

<div class="navbar-bg js-navbar-bg">
    <button class="navbar-bg__close"><i class="fa fa-angle-double-left" aria-hidden="true"></i></button>
</div>

<nav class="navbar-eda1 js-navbar">
    <div class="container">
        <div class="row">
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="navbar__collapse">
              <div class="navbar-eda1__city-check">
                
    <div class="b-form-select-pad" id="cityByIp">
        <div class="b-form-select js-select"><div class="b-form-select__active"><span class="js-current">Ханты-Мансийск</span></div><span class="b-form-select__drop-down"><span></span></span>
            <ul class="b-form-select__options js-options" style="display: none;">
                <li class="b-form-select__options__active-item js-current">Ханты-Мансийск</li>
                <li class="b-form-select__options__item" redirect-url="https://eda1.ru/?change_city=1" base-city-url="https://eda1.ru" style="display: none;">Ханты-Мансийск</li>
                <li class="b-form-select__options__item" redirect-url="http://pervouralsk.eda1.ru/?change_city=1" base-city-url="http://pervouralsk.eda1.ru/">Сургут</li>
                <!--li class="b-form-select__options__item" redirect-url="http://tagil.eda1.ru/?change_city=1" base-city-url="http://tagil.eda1.ru/">Нижний Тагил</li>
                <li class="b-form-select__options__item" redirect-url="http://kamensk.eda1.ru/?change_city=1" base-city-url="http://kamensk.eda1.ru/">Каменск-Уральский</li>
                <li class="b-form-select__options__item" redirect-url="http://chelyabinsk.eda1.ru/?change_city=1" base-city-url="http://chelyabinsk.eda1.ru/">Челябинск</li>
                <li class="b-form-select__options__item" redirect-url="http://tyumen.eda1.ru/?change_city=1" base-city-url="http://tyumen.eda1.ru/">Тюмень</li>
                <li class="b-form-select__options__item" redirect-url="https://www.eda1.ru/pyshma/?change_city=1" base-city-url="pyshma/index.html">Верхняя Пышма</li-->
            </ul>
        </div>
        <input class="js-input" type="hidden" value="pyshma/indexff67.html?change_city=1">
    </div>


    <script type="text/javascript">
        $(function(){
            $('#cityByIp').jsHtmlSelect({
                token: ""
            });
         })
    </script>


              </div>
<!--<ul class="nav navbar-nav">
<li class="dropdown">
  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Екатеринбург <span class="caret"></span></a>
  <ul class="dropdown-menu">
    <li><a href="#">Нижний Тагил</a></li>
  </ul>
</li>
</ul>-->
<ul class="navbar__list_rest js-swipe-menu">
                

    <li class="navbar__list_rest__item "><a href="yaponskaya-kuhnya.html" class="navbar__list_rest__item__link js-list_rest__item__link">Сушкоф</a>

        <ul class="navbar__list_rest__sub" style="display:none; " >

            <li class=""><a href="yaponskaya-kuhnya/novinki.html">Новинки</a></li>
            <li class=""><a href="pyshma/action.html" class="cat-page__menu__link">Филадельфия - Айс за 1 рубль</a></li>
            <li class=""><a href="pyshma/dostavka-sushi.html">Сеты</a></li>
            <li class=""><a href="pyshma/yaponskaya-kuhnya/rolly.html">Роллы</a></li>
            <li class=""><a href="pyshma/yaponskaya-kuhnya/rolly-philadelphia.html">Роллы Филадельфия</a></li>
            <li class=""><a href="pyshma/yaponskaya-kuhnya/gorjachie-rolly.html">Горячие роллы</a></li>
            <li class=""><a href="pyshma/yaponskaya-kuhnya/prostye-rolly.html">Простые роллы</a></li>
            <li class=""><a href="pyshma/yaponskaya-kuhnya/zapechennye-rolly.html">Запеченные роллы</a></li>
            <li class=""><a href="pyshma/yaponskaya-kuhnya/sushi.html">Суши и закуски</a></li>
            <li class=""><a href="pyshma/yaponskaya-kuhnya/yaponskie-salaty.html">Салаты</a></li>
            <li class=""><a href="pyshma/yaponskaya-kuhnya/6/247.html">Супы</a></li>
            <li class=""><a href="pyshma/yaponskaya-kuhnya/wok-menyu.html">Вок - меню</a></li>
            <li class=""><a href="pyshma/yaponskaya-kuhnya/deserty-iz-sushkof.html">Десерты</a></li>
            <li class=""><a href="pyshma/yaponskaya-kuhnya/napitki-iz-sushkof.html">Напитки</a></li>
            <li class=""><a href="pyshma/yaponskaya-kuhnya/dopolnitelno.html">Дополнительно</a></li>
        </ul>

    </li>
    <li class="navbar__list_rest__item "><a href="pyshma/picca.html" class="navbar__list_rest__item__link js-list_rest__item__link">Дель Песто</a>
        <ul class="navbar__list_rest__sub" style="display:none; " >
            <li class=""><a href="pyshma/picca/goryachee.html">Горячие блюда</a></li>
            <li class=""><a href="pyshma/picca/picca-30-sm.html">Пицца 30 см</a></li>
            <li class=""><a href="pyshma/picca/picca-40-sm.html">Пицца 40 см</a></li>
            <li class=""><a href="pyshma/picca/deserty-iz-del-pesto.html">Десерты</a></li>
            <li class=""><a href="pyshma/picca/napitki-iz-del-pesto.html">Напитки</a></li>
        </ul>
    </li>
    <li class="navbar__list_rest__item "><a href="pyshma/lapsha.html" class="navbar__list_rest__item__link js-list_rest__item__link">Лапша тетушки Бунси</a>

        <ul class="navbar__list_rest__sub" style="display:none; " >
            <li class=""><a href="pyshma/lapsha/domashnyaya-lapsha.html">Домашняя лапша</a></li>
            <li class=""><a href="pyshma/lapsha/napitki-ot-bunsi.html">Напитки</a></li>
            <li class=""><a href="pyshma/lapsha/deserty-ot-bunsi.html">Десерты</a></li>
        </ul>

    </li>
</ul>
<ul class="navbar__list">
    <li class="navbar__list__item_mob-show navbar__list__item_foot-show"><a href="pyshma/conditions.html">Условия доставки</a> </li>
    <li class="navbar__list__item_mob-show"><a href="pyshma/guestbook.html" >Оставить отзыв</a> </li>
    <li class="navbar__list__item_foot-show"><a href="pyshma/new_vacancy.html">Вакансии</a> </li>
    <li class="navbar__list__item_mob-show"><a href="pyshma/bonus_card.html">Бонусная карта</a> </li>
    <li class="navbar__list__item_mob-show"><a href="pyshma/contacts.html">Контакты</a> </li>
<li class=""><a href="pyshma/franchising.html" >Франчайзинг</a> </li>
    
</ul>
            </div><!-- /.navbar-collapse -->
        </div>
  </div><!-- /.container-fluid -->
</nav>
<div class="header">
    <div class="header__inner">
        <div class="container">
            <div class="row">
                <div class="header-menu" id="j_main">
                   <div class="row">
                        <div class="col-xs-4 header-menu__item ">
                            <a href="yaponskaya-kuhnya.html" class="header-menu__item__link header-menu__item__link_3" ><span>Японская кухня</span></a>
                        </div>
                        <div class="col-xs-4 header-menu__item ">
                            <a href="pyshma/picca.html" class="header-menu__item__link header-menu__item__link_6" ><span>Доставка пиццы</span></a>
                        </div>
                        <div class="col-xs-4 header-menu__item ">
                            <a href="pyshma/lapsha.html" class="header-menu__item__link header-menu__item__link_11" ><span>Паназиатская кухня</span></a>
                        </div>
                    </div>
                </div>

                <script type="text/javascript">
                    $(document).ready(function() {
                        var restaurant_id = ''; //3
                        $('#j_main').addClass('header-menu_' + restaurant_id);
                });
                </script>   

                <script type="text/javascript" src="js/view_phone.js"></script>
                <div class="header-contact hidden-xs hidden-sm hidden-md">
                    <div class="row">
                        <div class="col-xs-6 col-sm-4 text-center">
                            <div class="header-contact__phones-o">
                                <div class="header-contact__phones">
                                    <div class="header-contact__phones__item"><span class="header-contact__phones__item__title">&nbsp;</span> <span class="callibri_che_01">+7 3467 36 25 36</span></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-4 text-center">
                            <div class="header-contact__phones-o">
                                <div class="header-contact__phones">
                                    <div class="header-contact__phones__item"><span class="header-contact__phones__item__title">&nbsp;</span> <span class="callibri_che_02">+7 904 471 32 72</span></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-4 text-center">
                            <div class="header-contact__phones-o">
                                <div class="header-contact__phones">
                                    <div class="header-contact__phones__item"><span class="callibri_che_03"><span class="header-contact__phones__item__title">Горячая линия:</span> +7 904 471 32 72</span></div>
                                    <div class="header-contact__nocall">
                                        <a href="#" id="nocall-button" data-toggle="modal" data-target="#js-nocall-popup">Заказать звонок</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8 header-contact__time-info">
                                        <div class="header-contact__time">
                                Круглосуточно
                            </div>
                                    </div>
                        <!--<div class="col-lg-9">
                            <div class="header-contact__phones__callback"><a href="">ќбратный звонок</a></div>
                        </div>-->
                    </div>                  
                </div>
            </div>
        </div>
    </div>
</div>
<div class="header-basket">
    <div class="container">
        <div class="row">
            <button class="header__bars js-header-bars">
                <i class="fa fa-bars" aria-hidden="true"></i>
            </button>
            
                <script type="text/javascript">
                    var WindowHeight = $( window ).height();

                    $(".js-header-bars").click(function() {
                        $('.js-navbar-bg').show();
                        $('.js-navbar').show();
                        setTimeout(function(){
                            $('.navbar-bg').addClass('navbar-bg__open');
                        }, 100);
                        setTimeout(function(){
                            $('.navbar__collapse').addClass('navbar__collapse__open');
                        }, 100);
                        setTimeout(function(){
                            $('.navbar__collapse').addClass('navbar__collapse__opened');
                        }, 200);
                        $('html').css({"height": WindowHeight,'overflow-y':'hidden'});
                        $('body').css({"height": WindowHeight,'overflow-y':'hidden'});
                        $('.page').addClass('js-navbar-bg');
                    });
                    $(".js-navbar-bg").click(function() {
                        $('.navbar__collapse').removeClass('navbar__collapse__opened');
                        $('.navbar-bg').removeClass('navbar-bg__open');
                        $('.navbar__collapse').removeClass('navbar__collapse__open');
                        setTimeout(function(){
                            $('.js-navbar-bg').hide();
                            $('.js-navbar').hide();
                        }, 200);
                        $('html').css({"height": 'auto','overflow-y':'auto'});
                        $('body').css({"height": 'auto','overflow-y':'auto'});
                        $('.page').removeClass('js-navbar-bg');
                    });
                </script>
            
            <div class="text-center header__basket">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="b-basket-2">
                            <div class="order_basket al" id="basket_cont">
                                <a href="#" onclick="if(!$.cookie('step_order') || $.cookie('step_order') == 5){$.cookie('step_order', 1, {expires: 0,  path:  '/'});sendYandexMetric('go-to-basket');}  createOrder(); return false;" class="b-basket__open-popup  " id="basket_fly">
                                    <span class="b-basket__open-popup__title">Ваш заказ</span>
                                    <span class="b-basket__open-popup__price">0 <i class="fa fa-rub" aria-hidden="true"></i></span>
                                    <span class="b-basket__open-popup__count"></span>&nbsp;     </a>
                                <!--img src="/images/ico-basket.gif" alt="" title="" class="ico-basket" /-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="header__basket__call">
                <a href="tel:+7 904 471 32 72"></a>
            </div>
        </div>
    </div>
</div>
<!--div class="city-select-popup" id="js-city_select_popup">
    <div class="city-select-popup__inner">
        <form action="#" method="post" id="confirmCurrentCity">
            <div class="form-group">
                <div class="city-select-popup__current">Ваш город &ndash; <b>Екатеринбург</b></div>

                <div class="b-form-select-pad" id="citySelect" style="display: none;">
                    <div class="b-form-select js-select">
                        <div class="b-form-select__active">
                            <span class="js-current">Екатеринбург</span></div>
                        <span class="b-form-select__drop-down"><span></span></span>
                        <ul class="b-form-select__options js-options" style="display: none;">
                            <li class="b-form-select__options__active-item js-current">Екатеринбург</li>
<li class="b-form-select__options__item" data-val="https://eda1.ru/?change_city=1" style="display: none;">Екатеринбург</li>
                            <li class="b-form-select__options__item" data-val="http://pervouralsk.eda1.ru/?change_city=1">Первоуральск</li>
                            <li class="b-form-select__options__item" data-val="http://tagil.eda1.ru/?change_city=1">Нижний Тагил</li>
                            <li class="b-form-select__options__item" data-val="http://kamensk.eda1.ru/?change_city=1">Каменск-Уральский</li>
                            <li class="b-form-select__options__item" data-val="http://chelyabinsk.eda1.ru/?change_city=1">Челябинск</li>
                            <li class="b-form-select__options__item" data-val="http://tyumen.eda1.ru/?change_city=1">Тюмень</li>
                            <li class="b-form-select__options__item" data-val="pyshma/indexff67.html?change_city=1">Верхняя Пышма</li>
                                                                                    </ul>
                    </div>
                    <input class="js-input" type="hidden" value="https://eda1.ru/?change_city=1">
                </div>

                <div class="row">
                    <div class="col-xs-3">
                        <input type="button" class="btn btn-sm btn-primary btn-block" id="js-set-city" value="Ок">
                    </div>
                    <div class="col-xs-9">
                        <button class="btn btn-sm btn-primary btn-block btn__choice-another-city" onclick="$('.city-select-popup').addClass('city-select-popup_choice'); return false;">Выбрать другой город</button>
                    </div>
                </div>
            </div>
        </form>
            </div>
</div-->

<script type="text/javascript">
    $(function(){
        $('#citySelect').jsHtmlSelect();
//      $('#city_select_popup').show');
        $(document).on('click', '#js-set-city', function(){
            if($('#citySelect').is(':visible')){
                location.href = $('#citySelect .js-input').val();
            } else {
                //alert(location.hostname);
                $('#js-city_select_popup').hide();
            }
        });

        $.cookie('confirmCity', 0, {expires: 365,  path:  '/', domain: getMainHost()});

    })
</script>




    
<script type="text/javascript" src="js/view_phone.js"></script>



<script type="text/javascript">
    var size_global=6;
    var g_currentTimeH=11;
    var g_currentTimeM=03;
    var g_currentTimeS=19;
</script>
<script type="text/javascript" src="js/clock.js"></script>

<div class="home-img-banner">
            <div class="home-img-banner__item home-img-banner__item_left-bottom">
            <a href="pyshma/picca/picca-40-sm.html">
                <img src="images/new/Pizza-1066-.jpg" alt="" class="home-img-banner__small" />
                <span class="home-img-banner__small-true" data-trueimage="/images/new/Pizza-1066.jpg"></span>
                <span class="home-img-banner__big" data-trueimage="/images/new/Pizza-1920.jpg" style="background-image: url(images/new/Pizza-1920-.jpg);"></span>
                <span class="home-img-banner__big-true"></span>
            </a>
        </div>
        <div class="home-img-banner__item home-img-banner__item_left-top">
            <a href="pyshma/action.html">
                <img src="images/new/promo-1rub-min-.jpg" alt="" class="home-img-banner__small" />
                <span class="home-img-banner__small-true" data-trueimage="images/new/promo-1rub-min.jpg"></span>
                <span class="home-img-banner__big" data-trueimage="/images/new/promo-1rub-big.jpg" style="background-image: url(images/new/promo-1rub-big-.jpg);"></span>
                <span class="home-img-banner__big-true"></span>
            </a>
        </div>
        <div class="home-img-banner__item home-img-banner__item_left-bottom">
            <a href="yaponskaya-kuhnya.html">
                <img src="images/new/promo-sushkof-small2-.jpg" alt="" class="home-img-banner__small" />
                <span class="home-img-banner__small-true" data-trueimage="images/new/promo-sushkof-small2.jpg"></span>
                <span class="home-img-banner__big" data-trueimage="/images/new/promo-sushkof-big2.jpg" style="background-image: url(images/new/promo-sushkof-big2-.jpg);"></span>
                <span class="home-img-banner__big-true"></span>
            </a>
        </div>
        <div class="home-img-banner__item home-img-banner__item_left-bottom">
            <a href="pyshma/lapsha.html">
                <img src="images/new/promo-bunsi-small-.jpg" alt="" class="home-img-banner__small" />
                <span class="home-img-banner__small-true" data-trueimage="images/new/promo-bunsi-small.jpg"></span>
                <span class="home-img-banner__big" data-trueimage="/images/new/promo-bunsi-big.jpg" style="background-image: url(images/new/promo-bunsi-big-.jpg);"></span>
                <span class="home-img-banner__big-true"></span>
            </a>
        </div>
                            </div>

    <script type="text/javascript">
        function loadImageDfd(src) {
            var dfd = $.Deferred();
        
            var loadImg = new Image();
            loadImg.onload = function() {
                dfd.resolve();
            };
            loadImg.src = src;
            
            return dfd;
        }
        
        function loadImagesArray(images, callback) {
            var deffereds = [];
            for(var i=0; i<images.length; i++) {
                deffereds.push(loadImageDfd(images[i]));            
            }
            $.when.apply($, deffereds).done(callback);
        }
    
        $(function(){
            
            var images = [];

            var images1 = ['/images/new/header-menu_sushkof-mob.png','images/new/header-menu_delpesto-mob.png','/images/new/header-menu_bunsi-mob.png'];

            loadImagesArray(images1, function() {
            });
            
            $( ".home-img-banner__big,.home-img-banner__small-true" ).each(function( index ) {
                images[index] = $(this).attr('data-trueimage');
            });
            
            loadImagesArray(images, function() {
                imgLoaded(images);
            });
            
            function imgLoaded(images){
                $( ".home-img-banner__big" ).each(function( index ) {
                    $(this).next('.home-img-banner__big-true').css({
                        "background-image": "url(" + $(this).attr('data-trueimage') + ")"
                    });
                });
                $('.home-img-banner__big-true').addClass('home-img-banner__big-true_show');
                $( ".home-img-banner__small-true" ).each(function( index ) {
                    $(this).css({
                        "background-image": "url(" + $(this).attr('data-trueimage') + ")"
                    });
                });
                $('.home-img-banner__small-true').addClass('home-img-banner__small-true_show');
                setTimeout( function() {
                    $('.home-img-banner').owlCarousel({
                        loop:true,
                        margin:0,
                        autoplay:true,
                        smartSpeed: 1000,
                        autoplayTimeout:5000,
                        autoplayHoverPause:false,
                        nav:true,
                        responsive:{
                            0:{
                                items:1
                            }
                        }
                    }); 
                },700);
            };
        });
    </script>

<div class="main-bg">
    <div class="categories-list">
        <div class="container">
            <div class="categories-list__title">Свежая и разнообразная кухня</div>
            <div class="row">
                <div class="col-xs-6 col-lg-3"><div class="categories-list__item"><a href="pyshma/yaponskaya-kuhnya/sushi.html" class="categories-list__item__link"><img src="images/new/categories-list__item-sushi.png" class="categories-list__item__img"><span class="categories-list__item__name">Суши</span></a></div></div>
                <div class="col-xs-6 col-lg-3"><div class="categories-list__item"><a href="pyshma/yaponskaya-kuhnya/rolly.html" class="categories-list__item__link"><img src="images/new/categories-list__item-roll.png" class="categories-list__item__img"><span class="categories-list__item__name">Роллы</span></a></div></div>
                <div class="col-xs-6 col-lg-3"><div class="categories-list__item"><a href="pyshma/yaponskaya-kuhnya/gorjachie-rolly.html" class="categories-list__item__link"><img src="images/new/categories-list__item-hotroll.png" class="categories-list__item__img"><span class="categories-list__item__name">Горячие роллы</span></a></div></div>
                <div class="col-xs-6 col-lg-3"><div class="categories-list__item"><a href="pyshma/dostavka-sushi.html" class="categories-list__item__link"><img src="images/new/categories-list__item-set.png" class="categories-list__item__img"><span class="categories-list__item__name">Сеты</span></a></div></div>
                <div class="col-xs-6 col-lg-3"><div class="categories-list__item"><a href="pyshma/yaponskaya-kuhnya/6/248.html" class="categories-list__item__link"><img src="images/new/categories-list__item-philka.png" class="categories-list__item__img"><span class="categories-list__item__name">Роллы Филадельфия</span></a></div></div>
                <div class="col-xs-6 col-lg-3"><div class="categories-list__item"><a href="pyshma/picca.html" class="categories-list__item__link"><img src="images/new/categories-list__item-pizza.png" class="categories-list__item__img"><span class="categories-list__item__name">Пицца</span></a></div></div>
                <div class="col-xs-6 col-lg-3"><div class="categories-list__item"><a href="pyshma/lapsha.html" class="categories-list__item__link"><img src="images/new/categories-list__item-lapsha.png" class="categories-list__item__img"><span class="categories-list__item__name">Лапша</span></a></div></div>
                <div class="col-xs-6 col-lg-3"><div class="categories-list__item"><a href="pyshma/yaponskaya-kuhnya/wok-menyu.html" class="categories-list__item__link"><img src="images/new/categories-list__item-tyahan.png" class="categories-list__item__img"><span class="categories-list__item__name">Тяханы</span></a></div></div>
            </div>
        </div>
    </div>
    <div class="home-news">
        <div class="container">
                    <div class="row">
                <div class="col-sm-6 col-md-7">
                    <a href="pyshma/bonus_card.html" class="home-news__item" style="background-image: url(images/new/banner22.jpg);">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="home-news__item_time-skidka__bonus">Бонусная программа</div>
                                <div class="home-news__item_time-skidka__bonus-txt">Зарегистрироваться</div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-md-5">
                    <a href="pyshma/videocamera.html" class="home-news__item" style="background-image: url(images/new/banner33.jpg);">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="home-news__item_time-skidka__webcamera">Веб-камеры<br/>Смотрите приготовление<br/>онлайн!</div>
                            </div>
                        </div>
                    </a>
                </div>
                <!-- start test block -->
                <div class="col-sm-7 col-md-6">
                    <div class="home-news__item home-news__item_time-skidka" style="background-image: url(images/new/banner11.jpg);">
                        <div class="row">
                            <div class="col-sm-7">
                                <div class="home-news__item_time-skidka__title">Сейчас среднее<br/>время доставки</div>
                                <div class="home-news__item_time-skidka__value js-time-value-t">0</div>
                                <div class="home-news__item_time-skidka__unit">минут</div>
                                <div class="home-news__item_time-skidka__skidka-value">Сумма<br/>скидок сегодня</div>
                                <div class="home-news__item_time-skidka__skidka-unit"><span class="js-skidki-na-zakaz-t">0</span> <i class="fa fa-rub home-news__item_time-skidka__skidka-unit__rub" aria-hidden="true"></i></div>
                                
                                    <script type="text/javascript">
                                        $(function(){
                                            var ScrollTop = 0;
                                            var SkidkiNaZakaz = $( '.js-skidki-na-zakaz-t' ).offset();
                                            var TimeNaZakaz = $( '.js-time-value-t' ).offset();
                                            var WindowHeight = $( window ).height();
                                            var temp = 0;
                                            var temp1 = 0;
                                            $( window ).scroll(function() {
                                                ScrollTop = $( window ).scrollTop();
                                                if( (WindowHeight + ScrollTop - 100) > SkidkiNaZakaz.top ){
                                                    if ( temp == 0 ) {
                                                        temp = 1;
                                                        $('.js-skidki-na-zakaz-t').animateNumber({
                                
                                                            number: '1105'
                                
                                                        },2500);
                                                    }
                                                }
                                                if( (WindowHeight + ScrollTop - 100) > TimeNaZakaz.top ){
                                                    if ( temp1 == 0 ) {
                                                        temp1 = 1;
                                                        $('.js-time-value-t').animateNumber({
                                
                                                            number: 50
                                
                                                        },2500);
                                                    }
                                                }
                                            });
                                        });
                                    </script>
                                
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end test block -->
            </div>
                    </div>
    </div>
    <div class="home-blurbs">
        <div class="container">
            <div class="row">
                <div class="col-xs-6 col-lg-4">
                    <div class="home-blurbs__item">
                        <div class="home-blurbs__item__ico" style="background-image: url(images/new/ico1.png);">
                        </div>
                        <div class="home-blurbs__item__title">
                            Лосось самого высокого качества
                        </div>
                        <div class="home-blurbs__item__txt hidden-xs">
                            <p>А&nbsp;вы&nbsp;знаете, что у&nbsp;лосося есть три вида качества? Премиум, Грейд, Индастриал. Мы&nbsp;используем лосось только самого высшего качества Премиум. Только рыба качества Премиум может использоваться в&nbsp;суши.</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 col-lg-4">
                    <div class="home-blurbs__item">
                        <div class="home-blurbs__item__ico" style="background-image: url(images/new/ico2.png);">
                        </div>
                        <div class="home-blurbs__item__title">
                            Только полезная икра Тобико
                        </div>
                        <div class="home-blurbs__item__txt hidden-xs">
                            <p>Никаких заменителей Масаго! Икра Масаго&nbsp;&mdash; это икра мойвы, Тобико&nbsp;&mdash; икра летучей рыбы. У&nbsp;данных видов икры разный вкус и&nbsp;консистенция. Тобико менее распространенный и&nbsp;более дорогой продукт.</p>
                        </div>
                    </div>
                </div>
                <div class="clearfix visible-xs"></div>
                <div class="col-xs-6 col-lg-4">
                    <div class="home-blurbs__item">
                        <div class="home-blurbs__item__ico" style="background-image: url(images/new/ico3.png);">
                        </div>
                        <div class="home-blurbs__item__title">
                            Без консервантов
                        </div>
                        <div class="home-blurbs__item__txt hidden-xs">
                            <p>Мы&nbsp;не&nbsp;используем в&nbsp;приготовлении блюд усилители вкуса, пищевые добавки и&nbsp;консерванты. Только исключительно свежие ингредиенты, прошедшие контроль качества.</p>
                        </div>
                    </div>
                </div>
                <div class="clearfix visible-lg"></div>
                <div class="col-xs-6 col-lg-4">
                    <div class="home-blurbs__item">
                        <div class="home-blurbs__item__ico" style="background-image: url(images/new/ico4.png);">
                        </div>
                        <div class="home-blurbs__item__title">
                            Можем поменять Вам блюдо
                        </div>
                        <div class="home-blurbs__item__txt hidden-xs">
                            <p>Мы&nbsp;гарантируем высокое качество блюд. Если Вам что-то не&nbsp;понравилось, позвоните на&nbsp;горячую линию, и&nbsp;мы&nbsp;бесплатно быстро заменим непонравившееся блюдо.</p>
                        </div>
                    </div>
                </div>
                <div class="clearfix visible-xs"></div>
                <div class="col-xs-6 col-lg-4">
                    <div class="home-blurbs__item">
                        <div class="home-blurbs__item__ico" style="background-image: url(images/new/ico5.png);">
                        </div>
                        <div class="home-blurbs__item__title">
                            Доставка заказов вовремя
                        </div>
                        <div class="home-blurbs__item__txt hidden-xs">
                            <p>Мы&nbsp;всегда следим за&nbsp;тем, чтобы все наши заказы доставлялись вовремя, но&nbsp;если вдруг мы&nbsp;все-таки опоздали, то&nbsp;обязательно принесем извинения и&nbsp;сделаем скидку!</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 col-lg-4">
                    <div class="home-blurbs__item">
                        <div class="home-blurbs__item__ico" style="background-image: url(images/new/ico6.png);">
                        </div>
                        <div class="home-blurbs__item__title">
                            Идеальная чистота на&nbsp;кухне
                        </div>
                        <div class="home-blurbs__item__txt hidden-xs">
                            <p>Мы&nbsp;строго следим за&nbsp;санитарными правилами. В&nbsp;компании действует более 150 правил по&nbsp;соблюдению идеальных санитарных условий.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <!-- Modal -->
            <div class="modal fade news-slider-more" id="news-slider-more168" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="news-slider-more__item">
                            <div class="news-slider-more__item__img">
                                                            </div>
                            <div class="news-slider-more__item__date">
                                01.09.2016
                            </div>
                            <div class="news-slider-more__item__title">
                                Заказать на сайте быстрее, чем у оператора!
                            </div>
                            <div class="news-slider-more__item__txt">
                                <p>Уважаемые гости!</p>
<p>Рады сообщить вам, что заказ на сайте стал проще и быстрее, чем через оператора call-центра!</p>
<p>Наша "умная корзина" рассчитает и сообщит планируемое время доставки, предложит замену выбранным блюдам (если мы не сможем приготовить из-за отсутствия ингредиентов), посчитает скидки за самовывоз. Теперь весь процесс оформления заказа займет гораздо меньше времени.&nbsp;</p>
<p>01.09.2016</p>
<p>&nbsp;</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            <div class="modal fade news-slider-more" id="news-slider-more166" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="news-slider-more__item">
                            <div class="news-slider-more__item__img">
                                                            </div>
                            <div class="news-slider-more__item__date">
                                06.08.2016
                            </div>
                            <div class="news-slider-more__item__title">
                                Стань Тайным покупателем!
                            </div>
                            <div class="news-slider-more__item__txt">
                                <h3><strong>Уважаемые гости!</strong></h3>
<p>Мы стремимся предоставлять вам самые качественные блюда и радовать хорошим сервисом. Теперь мы просим вас помочь нам стать лучше.<br />Наши помощники называются Тайными покупателями. Тайный покупатель делает заказ в ресторане и проверяет качество блюд и сервиса по заранее определенным критериям.  Став нашим тайным покупателем, &nbsp;вы не только поможете &laquo;Сушкоф&raquo; и &laquo;Дель Песто&raquo; стать лучше, но и получите скидку на следующие заказы.</p>
<h3><strong>Как стать Тайным покупателем:</strong></h3>
<p>1.Регистрируйтесь на сайте <a href="mystery.html">https://www.eda1.ru/mystery</a>. <br />2.Записывайтесь в график проверок. В течение 3-х дней с вами свяжется наш сотрудник для подтверждения даты проверки. <br />3.Делаете необходимые аудиозаписи и фотографии, заполняете отчет в онлайн-кабинете.&nbsp;<br />4.Фотографируете блюда, заполняете отчет в онлайн-кабинете. <br />5.Когда отчет проходит проверку, вы получаете компенсацию 500!</p>
<h3><strong>Регистрируйтесь  на сайте <a href="mystery.html">https://www.eda1.ru/mystery</a>, чтобы все тайное стало явным!</strong></h3>
<p><a class="prettyPhoto" href="upload/images/support_gallery/mysteryshopper-3.jpg" onfocus="this.blur()"><img src="upload/images/support_gallery/mysteryshopper-3_sm.jpg" alt="" width="800" height="533" /></a></p>
<p>&nbsp;</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            <div class="modal fade news-slider-more" id="news-slider-more164" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="news-slider-more__item">
                            <div class="news-slider-more__item__img">
                                                            </div>
                            <div class="news-slider-more__item__date">
                                26.06.2016
                            </div>
                            <div class="news-slider-more__item__title">
                                Форель vs Лосось
                            </div>
                            <div class="news-slider-more__item__txt">
                                <p>Дорогие гости, <br />хотим сообщить вам важную новость: для приготовления японских блюд мы начинаем использовать форель.  <br />Традиционно стоимость форели была выше стоимости лосося. Поэтому раньше мы не могли позволить себе ее использование вместо лосося, ведь тогда нам пришлось бы поднять цены на блюда. Блюда с лососем  очень популярны среди наших гостей, и поднятие цен больно ударило бы по любителям роллов Филадельфия и других блюд с лососем. Но сейчас, из-за того, что после ввода экономических санкций привычные схемы поставки уже не работают, сложилась уникальная экономическая ситуация: закупка форели стала более выгодной, чем покупка лосося. Мы не уверены, что так будет всегда, но пока у нас есть возможность использовать в блюдах благородную деликатесную форель - мы будем это делать. <br />Форель,  как и лосось, относится к семейству лососевых. Филе форели может иметь беловато-желтоватый, розовый или ярко-красный оттенок, цвет прежде всего зависит от вида и среды обитания. Форель, которая импортируется в Россию, как правило, имеет более насыщенный  яркий  цвет. Калорийность форели ниже калорийности лосося, потому что форель содержит меньше жиров. <br />Обработка, посол и копчение форели технологически не отличается от обработки лосося. Мы в &laquo;Сушкоф&raquo; по-прежнему солим рыбу самостоятельно и гарантируем высокие стандарты качества обработки рыбы вне зависимости от того форель это или лосось. <br />Мы надеемся, что вам понравится это нововведение, и мы будем радовать вас форелью всегда, когда у нас будет такая возможность.</p>
<p>26.06.2016</p>
<p><img src="upload/images/support_gallery/Sushkof-banner-forel.png" alt="" width="956" height="345" /></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            <div class="modal fade news-slider-more" id="news-slider-more161" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="news-slider-more__item">
                            <div class="news-slider-more__item__img">
                                                            </div>
                            <div class="news-slider-more__item__date">
                                01.06.2016
                            </div>
                            <div class="news-slider-more__item__title">
                                Внимание! Бонусные баллы
                            </div>
                            <div class="news-slider-more__item__txt">
                                <p>Дорогие гости!<br /> <br />Приносим свои извинения за вынужденные неудобства тем владельцам бонусных карт, кто не обнаружил в личном кабинете заработанные бонусные баллы. Причиной тому является переход ресторана доставки на новую систему обработки заказов. Пожалуйста, не волнуйтесь, до конца этой недели мы восстановим  все данные по бонусным картам, и вы снова сможете использовать их для заказов в нашем ресторане доставки. <br />Также в личном кабинете пока не работает функция восстановления кода бонусной карты. Восстановить код бонусной карты вы можете, позвонив в колл-центр по любому из телефонов, указанных на сайте, в любой день с 11:00 до 00:00. Таким же способом вы можете уточнить количество начисленных вам баллов. Просим учесть, что звонок обязательно должен быть совершен с номера телефона, к которому привязана бонусная карта. <br />Мы обязательно известим вас о восстановлении всех функций бонусной системы на сайте дополнительно.</p>
<p>01 июня 2016</p>
<p><img src="upload/images/support_gallery/sorry-cat.jpg" alt="" width="800" height="545" /></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            <div class="modal fade news-slider-more" id="news-slider-more159" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="news-slider-more__item">
                            <div class="news-slider-more__item__img">
                                                            </div>
                            <div class="news-slider-more__item__date">
                                17.03.2016
                            </div>
                            <div class="news-slider-more__item__title">
                                Изменения в составе блюд
                            </div>
                            <div class="news-slider-more__item__txt">
                                <p>16.03.2016</p>
<p>Уважаемые гости! К сожалению, из-за санкций у нас временно нет стружки тунца. Мы не хотим лишать Вас любимых блюд, поэтому приготовим их для Вас с кунжутом вместо стружки тунца, если Вы этого пожелаете.&nbsp;</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            <div class="modal fade news-slider-more" id="news-slider-more170" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="news-slider-more__item">
                            <div class="news-slider-more__item__img">
                                                            </div>
                            <div class="news-slider-more__item__date">
                                04.01.2017
                            </div>
                            <div class="news-slider-more__item__title">
                                Мегафон, почините наши телефоны!
                            </div>
                            <div class="news-slider-more__item__txt">
                                <p>Уважаемые гости, в связи с аварией в компании Мегафон, которую в течении двух суток не могут устранить - у нас работают не все номера телефонов, указанные в буклетах. &nbsp;На сайте мы оставили только рабочие номера.&nbsp;</p>
<p>Мегафон, почините наши телефоны!</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
    
        <script type="text/javascript">
            $(function(){
                if($(window).width()<992){
                    $('#news-slider').owlCarousel({
                        center: true,
                        items:2,
                        loop:true,
                        margin:10,
                        nav:false,
                        dots:false
                    });
                } else {
                    $('#news-slider').owlCarousel({
                        center: false,
                        items:2,
                        loop:true,
                        margin:34,
                        nav:true,
                        dots:false
                    });
                }       
            });
        </script>
    


    
    <script type="text/javascript">
        var is_off_order=0;
    </script>
    
        <script type="text/javascript">
            $(function(){

    //          $.post(GLOBAL_BASE_URL+"/ajax/handbook/getLastOrderProducts", {}, function($_options) {
    //              $('.last_order_items').html($_options);
    //              $('.last_order_items .product-container:lt(8)').appendTo($('#guest-last-choice-slider'));
    //              initCarousel();
    //              setInterval(updateLastOrderCarousel, 2000);
    //          });

    //          function updateLastOrderCarousel(){
    //              если из не показанных осталось мало - берем новые
    //              if($('.last_order_items .product-container').length < 3){
    //                  $.post(GLOBAL_BASE_URL+"/ajax/handbook/getLastOrderProducts", {}, function($_options) {
    //                      $('.last_order_items').append($_options);
    //                  });
    //              }
    //
    //              вставляем в начало первый из скрытых товаров
    //              var firstItemContainer = $('#guest-last-choice-slider .owl-item').first();
    //              var item = $('.last_order_items .product-container').first();
    //              firstItemContainer.clone().html(item).insertBefore(firstItemContainer);
    //              $('#guest-last-choice-slider .owl-item.active').last().removeClass('active');
    //              удаляем последний
    //              $('#guest-last-choice-slider .owl-item').last().remove();
    //          }

                initCarousel();

                function initCarousel(){
                    if($(window).width()<768){
                        $('#guest-last-choice-slider').owlCarousel({
                            center: true,
                            items:2,
                            loop:false,
                            margin:15,
                            nav:false,
                            dots:false
                        });
                    } else if($(window).width()<992){
                        $('#guest-last-choice-slider').owlCarousel({
                            center: true,
                            items:3,
                            loop:false,
                            margin:34,
                            nav:false,
                            dots:false
                        });
                    } else {
                        $('#guest-last-choice-slider').owlCarousel({
                            center: false,
                            items:4,
                            loop:false,
                            margin:22,
                            nav:true,
                            dots:false
                        });
                    }
                }
            });
        </script>
    
    
    
        <script type="text/javascript">
            $(function(){
                if($(window).width()<992){
                    $('#flamp-slider').owlCarousel({
                        center: true,
                        items:2,
                        loop:true,
                        margin:34,
                        nav:false,
                        dots:false
                    });         
                } else {
                    $('#flamp-slider').owlCarousel({
                        center: false,
                        items:3,
                        loop:true,
                        margin:34,
                        nav:true,
                        dots:false
                    });         
                }       
            });
        </script>
    
    <div class="all-page-txt hidden-xs">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Ресторан доставки EDA1.RU &ndash; вся гастрономия мира для жителей Екатеринбурга</h1>
<p>Наш сайт позволяет заказать быструю <strong>доставку блюд на дом</strong> для всех жителей и гостей города. Наша круглосуточная служба действительно является номером один для екатеринбуржцев, так как на сайте мы объединили самые известные в городе заведения, которые предлагают на заказ разнообразные национальные кухни разных стран мира.</p>
<h2>Заказ еды круглосуточно на дом или в офис: быстро и вкусно!</h2>
<p>Все представленные у нас заведения гарантируют отличный вкус предлагаемых блюд, которые готовятся только из отборных и свежих ингредиентов. Информация о способах и времени доставки, а также стоимости заказа еды на дом или в офис содержится на сайте. Вне зависимости от удаленности указанного гостем адреса в Екатеринбурге, наши курьеры оперативно привезут заказанные блюда. В случае их опоздания, предусмотрены скидки.</p>
                </div>
                <div class="col-md-12">
                    <a href="nashi-adresa.html">Наши адреса</a>
                </div>
            </div>
        </div>
    </div>
</div>




<div class="b-ad">
    <div class="b-ad_inner">
        <a class="b-ad_link" target="_blank" href="https://play.google.com/store/apps/details?id=ru.eda1.Sushkof"></a>
        <a class="b-ad_close" href="#"></a>
    </div>
</div>


<div class="footer-menu">
    <div class="container">
        <ul class="footer-menu__list">
            
        
                    <li class="navbar__list__item_mob-show navbar__list__item_foot-show"><a href="pyshma/conditions.html">Условия доставки</a> </li>
    
    
    <li class="navbar__list__item_mob-show"><a href="pyshma/guestbook.html" >Оставить отзыв</a> </li>
    <li class="navbar__list__item_foot-show"><a href="pyshma/new_vacancy.html">Вакансии</a> </li>
        
                    <li class="navbar__list__item_mob-show"><a href="pyshma/bonus_card.html">Бонусная карта</a> </li>
        
                    <li class="navbar__list__item_mob-show"><a href="pyshma/contacts.html">Контакты</a> </li>
    
    

    

    
    

            <li class=""><a href="pyshma/franchising.html" >Франчайзинг</a> </li>
    
     
            <li class="footer-menu__last-empty">&nbsp;</li>
        </ul>
    </div>
</div>
<footer class="footer" itemscope itemtype="http://schema.org/Organization">
    <div class="container">
        <div class="row">
            <div class="col-md-5 col-lg-3 hidden-xs hidden-sm">
                <div class="footer__cont">
                                        <div class="footer__copyright">
                                                ©2008-2017 <span itemprop="name">Служба доставки еды EDA1.RU — доставка и заказ еды на дом в Екатеринбурге</span>
                                                <br>
                        Все торговые знаки зарегистрированы. Екатеринбург, 2012 г.<br/>
                    </div>
                    <div class="footer__map"><a href="sitemap.html">Карта сайта</a></div>
                    <div class="footer__map"><a href="#" class="footer__mobile-ver js-site-desktop">Мобильная версия</a></div>
                </div>
            </div>
            <div class="col-md-7 col-lg-6">
                <div class="footer__contact-cont">
                    <div class="footer__adr hidden-xs" itemscope="itemscope" itemtype="http://schema.org/PostalAddress">Наш адрес: г. Екатеринбург, ул. Орджоникидзе, д. 3 
<br />Телефон: +7 (343) 373-44-44</div>
                    <div class="row">
    <div class="col-md-5 footer__phone" itemprop="telephone">
        <span class="callibri_che_02">+7 922 023 99 99</span>
    </div>
    <div class="col-md-7 footer__hot" itemprop="telephone">
        <span class="callibri_che_03"><span class="footer__hot__title">Горячая линия:</span> +7 800 700 56 11</span>
    </div>
    <div class="col-md-5 footer__phone" itemprop="telephone">
        <span class="callibri_che_01">+7 343 373 44 44</span>
    </div>
    <div class="col-md-7 footer__callback">
        <span><a href="#" id="nocall-button" data-toggle="modal" data-target="#js-nocall-popup">Заказать звонок</a></span>
    </div>
</div>              </div>
            </div>
            <div class="col-md-12 col-lg-3">
                <div class="footer__soc-cont">
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            <ul class="footer__soc">
                                <li class="footer__soc__item"><a href="http://www.facebook.com/eda1.ru" target="_blank" class="footer__soc__link footer__soc__link_f"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                <li class="footer__soc__item"><a href="https://twitter.com/Sushkof" target="_blank" class="footer__soc__link footer__soc__link_t"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                <li class="footer__soc__item"><a href="http://instagram.com/eda1.ru" target="_blank" class="footer__soc__link footer__soc__link_i"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                <li class="footer__soc__item"><a href="https://vk.com/sushkof" target="_blank" class="footer__soc__link footer__soc__link_v"><i class="fa fa-vk" aria-hidden="true"></>
                                <li class="footer__soc__item"><a href="http://ok.ru/sushkof" target="_blank" class="footer__soc__link footer__soc__link_ok"><i class="fa fa-odnoklassniki" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                        <div class="col-lg-12 footer__map"><a href="#" class="footer__desktop-ver js-site-desktop">Полная версия</a></div></div>
                                            </div>
                </div>
            </div>
        </div>
    </div>

</footer>
</div>
</body>