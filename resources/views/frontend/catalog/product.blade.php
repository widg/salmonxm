<script type="text/javascript">
    $(function(){
        $('#citySelect').jsHtmlSelect();
//      $('#city_select_popup').show');
        $(document).on('click', '#js-set-city', function(){
            if($('#citySelect').is(':visible')){
                location.href = $('#citySelect .js-input').val();
            } else {
                //alert(location.hostname);
                $('#js-city_select_popup').hide();
            }
        });

        $.cookie('confirmCity', 0, {expires: 365,  path:  '/', domain: getMainHost()});

    })
</script>


<script type="text/javascript">
    var is_off_order=0;
</script>

<script type="text/javascript">
    $(function(){
        Ecommerce.enterCategory();
    });
</script>



<div class="cat-page">
    <div class="cat-page__container">
        <div class="row">
            <div class="col-lg-3 cat-page__sidebar">
                <div class="hidden-xs hidden-sm hidden-md">
                <div class="cat-page__menu__title">Меню</div>
                <div class="cat-page__menu">
                    <ul class="cat-page__menu">
                        <li class="cat-page__menu__item">
                            <a href="/pizza" class="cat-page__menu__link">Пицца</a>

                        </li>
                        <li class="cat-page__menu__item">
                            <a href="/sushi" class="cat-page__menu__link">Роллы</a>

                        </li>
                        {{--

                        <li class="cat-page__menu__item">
                            <a href="/gorjachie-rolly" class="cat-page__menu__link">Горячие блюда</a>
                        </li>
                        <li class="cat-page__menu__item">
                            <a href="/deserty" class="cat-page__menu__link">Десерты</a>
                        </li>
                        <li class="cat-page__menu__item">
                            <a href="/sausages" class="cat-page__menu__link">???</a>
                        </li>
                        <li class="cat-page__menu__item">
                            <a href="/sety" class="cat-page__menu__link">Сеты</a>
                        </li>
                        <li class="cat-page__menu__item">
                            <a href="/zakuski" class="cat-page__menu__link">Закуски</a>
                        </li>
                        <li class="cat-page__menu__item">
                            <a href="/zapechennye-rolly" class="cat-page__menu__link">Запечённые роллы</a>
                        </li>
                        <li class="cat-page__menu__item">
                            <a href="/dop" class="cat-page__menu__link">Дополнительно</a>
                        </li>
                        <li class="cat-page__menu__item">
                            <a href="/drinks" class="cat-page__menu__link">Напитки</a>

                        </li>--}}
                    </ul>
                </div>
                                
                <script type="text/javascript">
                    $(document).ready(function(){
                        var ScrollTop = 0;
                        var WindowHeight = $( window ).height();
                        var MenuHeight = $( 'ul.cat-page__menu' ).height();
                        $( 'div.cat-page__sidebar' ).css("minHeight", MenuHeight);
                        var MenuWidth = $( 'ul.cat-page__menu' ).width();
                        var DocumentHeight = $( document ).height();
                        var HeightSumms = MenuHeight + 284 + 120;
                        $( window ).scroll(function() {
                            ScrollTop = $( window ).scrollTop();
                            if (MenuHeight<WindowHeight-120){
                                if (ScrollTop>99){
                                    $( 'div.cat-page__menu' ).addClass('cat-page__menu__fixed');
                                    $( 'div.cat-page__menu ul.cat-page__menu' ).width(MenuWidth);
                                    if (HeightSumms > (DocumentHeight-ScrollTop) ){
                                        $( 'div.cat-page__menu' ).addClass('cat-page__menu__fixed-bottom');
                                    } else {
                                        $( 'div.cat-page__menu' ).removeClass('cat-page__menu__fixed-bottom');
                                    }
                                } else {
                                    $( 'div.cat-page__menu' ).removeClass('cat-page__menu__fixed');
                                    $( 'div.cat-page__menu ul.cat-page__menu' ).width('auto');
                                }
                            }
                        });
                        
                        
                        $( ".js-cat-page__menu__show" ).click(function() {
                            $('ul.cat-page__menu').toggle();
                        });
                    });
                </script>

                </div>
            </div>
        <div class="col-lg-9 cat-page__main">
            <div class="content" id="result" data-category="Лучшие предложения">
                <h2 class="cat-page__h2">Новинки</h2>

                <div class="row">


    @include('frontend.layouts.getProducts')


                </div>

				{{--

                <div class="cat-page__txt visible-lg">
                    <div style="text-align: justify;">
                    <h1 style="font-family: Verdana;font-size: large; margin-bottom: 15px; color: #e00a1a;">&laquo;Сушкоф&raquo; - настоящая и недорогая японская кухня</h1>
                    <p>Традиционная кухня Японии не так давно еще была экзотической для европейской части континента, а сейчас твердо вошла и в меню российских суши-баров и кафе. Это одна из очень полезных кухонь, основой которой считается только правильный отбор ингредиентов, основывающих любое взятое блюдо. Входящие в состав специальный рис, свежая рыба, морепродукты, водоросли нори, грибы, фрукты и овощи добавляют особый вкус пище. В ресторанах &laquo;Сушкоф&raquo; каждый элемент приготавливаемого блюда старательно отбирается. Основная цель наших шеф-поваров &ndash; сохранность первичных качеств продуктов и свежесть доставляемой японской еды на заказ клиентам.</p>
                    <p>У нас большой выбор блюд в меню японской кухни на заказ: <a title="Заказать сет суши и роллов" href="pyshma/dostavka-sushi.html">суши-сеты</a>, <a title="Заказать вкусные роллы" href="pyshma/yaponskaya-kuhnya/rolly.html">вкусные роллы</a>,&nbsp;<a title="Заказать японский салат" href="pyshma/yaponskaya-kuhnya/yaponskie-salaty.html">салаты</a>, <a title="Заказать wok" href="pyshma/yaponskaya-kuhnya/wok-menyu.html">wok</a>. Мы привезем их Вам в любой район Екатеринбурга в любое время суток. Заказать доставку блюд японской кухни на дом в Екатеринбурге можно на сайте ресторана-доставки &quot;Еда1.ру&quot; и по телефону. Цены в нашем меню самые недорогие. Служба доставки суши &laquo;Сушкоф&raquo; работает круглосуточно и быстро!</p>
                    <h2 style="font-family: Verdana;font-size: large; margin-bottom: 15px; color: #e00a1a;">Почему нужно заказывать именно в &laquo;Сушкоф&raquo;?</h2>
                    <p>Мы гарантируем, что все наши японские блюда не готовятся заранее!</p>
                    <p>Мы всегда следим за тем, чтобы все наши заказы японской еды доставлялись вовремя, а лучше даже раньше обещанного времени. Если вдруг курьер все-таки опоздал, то обязательно принесем извинения и сделаем скидку, по <a title="Золотые правила" href="pyshma/conditions.html">золотым правилам</a>.</p>
                    <p>Мы работаем 5 лет и являемся подразделением компании, которая существует на рынке более 15 лет и управляет уже ставшими культовыми ресторанами &laquo;Конкиста&raquo;, &laquo;Куршевель&raquo; и &laquo;Ирландский дворик&raquo;. Поэтому, поверьте, мы не будем экономить на качестве нашей работы. Мы работаем только с  надежными поставщиками и используем только те продукты, которые имеют все необходимые сертификаты и ветеринарные справки. Мы не позволим себе готовить на арендованных квартирах, как это иногда происходит в других компаниях. Мы используем только специализированные производственные площади, соответствующие всем санитарным нормам.</p>
                    <p>К приготовлению ваших заказов мы допускаем поваров только после получения санитарной книжки, поэтому вы можете не опасаться за свое здоровье, заказывая у нас доставку блюд японской кухни.</p>
                    <p>Только в нашей службе вы можете сами выбрать, какой лосось будет использоваться в приготовлении заказанных вами блюд - слабосоленый или слабокопченый. Оба этих варианта более безопасны, чем обычный лосось. Слабосоленый лосось получают путем соления в течение 12 часов. Слабокопченый же лосось в течение строго определенного времени коптят по собственной технологии с ольховой стружкой. И никаких пищевых добавок и приправ!</p>
                    <p>Если Вы владелец бонусной карты нашего ресторана, то при оформлении любого заказа с доставкой, Вам начисляются баллы после каждого заказа. Накопленные баллы вы можете в любой момент потратить на оплату заказа и купить японскую еду совершенно бесплатно. <a title="О бонусной карте" href="pyshma/bonus_card.html">Подробнее о бонусной карте</a>.</p>
                    <p>Приятного аппетита!</p>
                    </div>
                </div>
				--}}
            </div>
        </div>
    </div>
</div>
</div>