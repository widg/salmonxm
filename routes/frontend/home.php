<?php

/**
 * Frontend Controllers
 * All route names are prefixed with 'frontend.'.
 */

Route::post('/user/buy', 'HomeController@buyProducts2');

//Route::get('/', 'HomeController@index')->name('index');
Route::get('/', function () {
    return redirect('sushi');
});
Route::get('contact', 'ContactController@index')->name('contact');
Route::post('contact/send', 'ContactController@send')->name('contact.send');

Route::group(['middleware' => 'web'], function () {
    //Route::post('/register', 'Auth\RegisterController@register');
    //Route::post('/login', 'Auth\LoginController@login');
    //Route::get('/logout', 'Auth\LoginController@logout');
    Route::get('/cart/add_to_cart', 'HomeController@addToCart');
    Route::get('/cart/delete_from_cart', 'HomeController@deleteFromCart');
});
//bonus_card
Route::get('/bonus_card', 'PageController@bonus_card');
//new_vacancy
Route::get('/new_vacancy', 'PageController@new_vacancy');
//conditions
Route::get('/conditions', 'PageController@conditions');
Route::get('/terms-use', 'PageController@termsuse');
//guestbook
Route::get('/guestbook', 'PageController@guestbook');
Route::get('/guestbooks', 'PageController@guestbooks');
Route::post('/guestbook', 'PageController@addguestbook');

/*
 * These frontend controllers require the user to be logged in
 * All route names are prefixed with 'frontend.'
 * These routes can not be hit if the password is expired
 */
Route::group(['middleware' => ['auth', 'password_expires']], function () {
    Route::group(['namespace' => 'User', 'as' => 'user.'], function () {
        /*
         * User Dashboard Specific
         */
        Route::get('dashboard', 'DashboardController@index')->name('dashboard');

        /*
         * User Account Specific
         */
        Route::get('account', 'AccountController@index')->name('account');

        /*
         * User Profile Specific
         */
        Route::patch('profile/update', 'ProfileController@update')->name('profile.update');
    });
});

Route::get('/{category}', 'HomeController@showCategory')->where('category', 'pizza|sushi|drinks|sausages|gorjachie-rolly|sety|rolly|deserty|zakuski|zapechennye-rolly|dop');
Route::get('/{category}/{id}', 'HomeController@show')->where('category', 'pizza|sushi|drinks|sausages|gorjachie-rolly|sety|rolly|deserty|zakuski|zapechennye-rolly|dop');