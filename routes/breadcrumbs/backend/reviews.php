<?php
/**
 * Created by PhpStorm.
 * User: nsd
 * Date: 08.04.2018
 * Time: 17:27
 */

Breadcrumbs::register('admin.reviews', function ($breadcrumbs) {
    $breadcrumbs->push(('Отзывы'), route('admin.reviews'));
});