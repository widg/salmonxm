<?php

Breadcrumbs::register('admin.products', function ($breadcrumbs) {
    $breadcrumbs->push(('Каталог продуктов'), route('admin.products'));
});

Breadcrumbs::register('admin.product_add', function ($breadcrumbs) {
    $breadcrumbs->push(('Добавить продукт'), route('admin.product_add'));
});
