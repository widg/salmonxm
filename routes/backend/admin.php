<?php

/**
 * All route names are prefixed with 'admin.'.
 */
Route::redirect('/', '/admin/dashboard', 301);
Route::get('dashboard', 'DashboardController@index')->name('dashboard');
Route::get('products', 'DashboardController@products')->name('products');
Route::get('product_add', 'DashboardController@product_add')->name('product_add');
Route::get('reviews', 'DashboardController@reviews')->name('reviews');
